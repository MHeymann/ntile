### DIRS ##################################################################
SRC_DIR = src
OBJ_DIR = obj

### OBJS ##################################################################
PARSER_OBJS = $(OBJ_DIR)/parser.o
STATE_OBJS = $(OBJ_DIR)/state.o
GAME_OBJS = $(OBJ_DIR)/game.o $(STATE_OBJS) $(HEUR_OBJS)
QUEUE_OBJS = $(OBJ_DIR)/queue.o
HASH_OBJS = $(OBJ_DIR)/hashtable.o
HSET_OBJS = $(OBJ_DIR)/hashset.o $(HASH_OBJS) $(GAME_OBJS)
WALKING_OBJS = $(OBJ_DIR)/walking/walking.o $(OBJ_DIR)/walking/w_hashset.o $(OBJ_DIR)/walking/place.o $(OBJ_DIR)/walking/loader.o $(HASH_OBJS) $(QUEUE_OBJS) $(OBJ_DIR)/walking/w_state.o
HEUR_OBJS = $(OBJ_DIR)/heuristic.o $(STATE_OBJS) $(WALKING_OBJS)

OBJS = $(QUEUE_OBJS)
TESTEXES = testparser teststate testgame testqueue testhash testhashset test_m
EXES = serialsolver parallelsolver ida pthreadsolver openmpsolver

### FLAGS #################################################################
CFLAGS = -Wall -Wextra -pedantic -g -O3
DBGFLAGS = -DDEBUG #-DDEBUGQ #-DDEBUGN #-DDEBUGS #-DDEBUGI
LFLAGS = -lm 

### COMMANDS ##############################################################
CC 		=  gcc
MPICC 	=  mpicc
RM 		= rm -f
COMPILE = $(CC) $(CFLAGS) $(DBGFLAGS)
MPICOMP = $(MPICC) $(CFLAGS) $(DBGFLAGS)


### RULES #################################################################

all: clean $(EXES) run_walk

tests: $(TESTEXES)


testparser: $(SRC_DIR)/testparser.c $(PARSER_OBJS)
	$(COMPILE) -o $@ $^ $(LFLAGS)

teststate: $(SRC_DIR)/teststate.c $(STATE_OBJS) $(PARSER_OBJS) $(HEUR_OBJS)
	$(COMPILE) -o $@ $^ $(LFLAGS)

testgame: $(SRC_DIR)/testgame.c $(GAME_OBJS) $(PARSER_OBJS)
	$(COMPILE) -o $@ $^ $(LFLAGS)

testqueue: $(SRC_DIR)/testqueue.c $(QUEUE_OBJS) $(GAME_OBJS) $(PARSER_OBJS)
	$(COMPILE) -o $@ $^ $(LFLAGS)

testhash: $(SRC_DIR)/testhashtable.c $(HASH_OBJS) $(PARSER_OBJS)
	$(COMPILE) -o $@ $^ $(LFLAGS)

testhashset: $(SRC_DIR)/testhashset.c $(HSET_OBJS) $(PARSER_OBJS)
	$(COMPILE) -o $@ $^ $(LFLAGS)

serialsolver: $(SRC_DIR)/serialsolver.c $(QUEUE_OBJS) $(HSET_OBJS) $(GAME_OBJS) $(PARSER_OBJS) $(HEUR_OBJS)
	$(COMPILE) -o $@ $^ $(LFLAGS)

pthreadsolver: $(SRC_DIR)/pthreadsolver.c $(QUEUE_OBJS) $(HSET_OBJS) $(GAME_OBJS) $(PARSER_OBJS) $(HEUR_OBJS)
	$(COMPILE) -o $@ $^ $(LFLAGS) -pthread

openmpsolver: $(SRC_DIR)/openmpsolver.c $(QUEUE_OBJS) $(HSET_OBJS) $(GAME_OBJS) $(PARSER_OBJS) $(HEUR_OBJS)
	$(COMPILE) -fopenmp -o $@ $^ $(LFLAGS)

ida: $(SRC_DIR)/ida.c $(QUEUE_OBJS) $(GAME_OBJS) $(PARSER_OBJS) $(HEUR_OBJS)
	$(COMPILE) -o $@ $^ $(LFLAGS)

parallelsolver: $(SRC_DIR)/parallelsolver.c $(QUEUE_OBJS) $(HSET_OBJS) $(GAME_OBJS) $(PARSER_OBJS) $(HEUR_OBJS)
	$(MPICOMP) -o $@ $^ $(LFLAGS)

test_m: $(SRC_DIR)/test_m.c $(QUEUE_OBJS) $(HSET_OBJS) $(GAME_OBJS) $(PARSER_OBJS) $(HEUR_OBJS)
	$(COMPILE) -o $@ $^ $(LFLAGS)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c
	$(COMPILE) -c -o $@ $^

.PHONY: clean
clean:
	$(RM) $(OBJS) $(EXES) $(TESTEXES)
	$(RM) $(OBJ_DIR)/*.o
	$(RM) $(OBJ_DIR)/walking/*.o

.PHONY: run_walk
run_walk: 
	make -C src clean
	make -C src DBGFLAGS=
	./src/getdb 4 > 4walk.db

