#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>

#include "parser.h"
#include "game.h"
#include "queue.h"
#include "hashset.h"
#include "heuristic.h"

/*** Global static variables *********************************************/

static int hs_init_delta = 24;
static int hs_init_diff = 2;

static char heuristic = WALKING;
#ifdef DEBUG
static int count_all_nodes = 0;
#endif

/*** Helper Function Prototypes ******************************************/

void get_args(int argc, char *argv[]);
game_t *open_game(char *filename, char *N);
int init_q_hs(queue_t **q, hashset_ptr *hs, game_t *game);
game_t *find_puzzle_solution(queue_t *q, hashset_ptr hs, int *local_c);
void print_solution(game_t *start, game_t *solution);

/* queue functions */
int cmp_n(void *n1, void *n2);
void free_n(void *n);
void print_g(void *n);
int get_weight(void *n);

/*** Main Routine ********************************************************/

int main(int argc, char *argv[])
{
	queue_t *q = NULL;
	hashset_ptr hs = NULL;
	game_t *game = NULL;
	game_t *start = NULL;
	game_t *mid = NULL;
	char N = 0;
	int local_c = INT_MAX;
	
	get_args(argc, argv);

	if (!(start = open_game("tests/brian.txt", &N))) {
		fprintf(stderr, "Something went wrong allocating the local structures\n");
		goto CLEANUP;
	}
	if (!(mid = open_game("tests/midbrian.txt", &N))) {
		fprintf(stderr, "Something went wrong allocating the local structures\n");
		goto CLEANUP;
	}
	if (!init_q_hs(&q, &hs, mid)) {
		fprintf(stderr, "Something went wrong allocating the local structures.\n");
		goto CLEANUP;
	}

	game = find_puzzle_solution(q, hs, &local_c);
	start->move_count = 0;

	print_solution(start, game);
#ifdef DEBUG
	printf("Total nodes: %d\n", count_all_nodes);
#endif
	
CLEANUP:
	/* Free data structures */
	/*
	 * game is in the hashset and will be free'd accordingly 
	 */
	if (start) {
		free_game(start);
		start = NULL;
	}
	if (mid) {
		free_game(mid);
		mid = NULL;
	}
	if (q) {
		free_queue(q);
	}
	if (hs) {
		free_hashset(hs);
	}
	free_walking();

	/* no segfaults or early exits */
#ifdef DEBUG
	printf("exiting normally\n");
#endif
	return 0;
}

/*** Helper Functions ****************************************************/

void get_args(int argc, char *argv[])
{
	int i;
	char *s;
	for (i = 2; i < argc; i++) {
		s = argv[i];
		if (strncmp(argv[i], "-heur=", 6) == 0) {
			if (strncmp((s + 6), "man", 3) == 0) {
				heuristic = MANHATTAN;
			} else {
				heuristic = WALKING;
			}
		} else if (strncmp(argv[i], "-delta_diff=", 12) == 0) {
			hs_init_diff = atoi(s + 12);
		} else if (strncmp(argv[i], "-delta_init=", 12) == 0) {
			hs_init_delta = atoi(s + 12);
		}
	}

}

game_t *open_game(char *filename, char *N)
{
	char *buffer = NULL;
	game_t *game = NULL;

	if (!filename || !N) {
		fprintf(stderr, "please provide valid pointers to open_game\n");
		return NULL;
	}

	game = new_game();
	if (!game) {
		fprintf(stderr, "Memory error\n");
		return NULL;
	}

	read_file(filename, &buffer, N);
	if (!buffer) {
		printf("Please provide a valid input file. \n");
		free_game(game);
		return NULL;
	}
	if (heuristic == WALKING) {
		init_walking(*N);
		printf("yes\n");
	}
	set_game(game, buffer, *N, 10, "RRUULLDRRU", heuristic);

	return game;
}

int init_q_hs(queue_t **q, hashset_ptr *hs, game_t *game)
{
	game_t *ret_game = NULL;

	if (!q || !hs || !game) {
		fprintf(stderr, "please provide valid pointers to alloc_structures\n");
		return FALSE;
	}

	*q = NULL;
	*hs = NULL;

	/* initialise local structures */
	
	ret_game = new_game();
	if (!ret_game) {
		fprintf(stderr, "Memory error in init\n");
		free_game(game);
		goto ATTEMPT_CLEANUP;
	}

	copy_game(game, ret_game);

	init_queue(q, cmp_n, free_n);
	if (!(*q)) {
		fprintf(stderr, "Failed to initialise queue\n");
		free_game(ret_game);
		goto ATTEMPT_CLEANUP;
	}

	hashset_init(hs, hs_init_delta, hs_init_diff, ALL);
	if (!(*hs)) {
		fprintf(stderr, "Failed to initialise hashset\n");
		free_game(ret_game);
		goto ATTEMPT_CLEANUP;
	}
	if (!insert_node(*q, ret_game)) {
		free_game(ret_game);
		goto ATTEMPT_CLEANUP;
	}
	/*
	 */
	if (!hashset_insert(*hs, ret_game)) {
		free_queue(*q);
		*q = NULL;
		goto ATTEMPT_CLEANUP;
	}

	if (is_solveable((state_t *)game)) {
		return TRUE;
	} else {
		printf("Unsolveable problem was loaded.\n");
	}

ATTEMPT_CLEANUP:
	
	if (*q) {
		ret_game = pop_first(*q);
		free_queue(*q);
		*q = NULL;
	}
	/* the next line should free the game popped above */
	if (*hs) {
		free_hashset(*hs);
		*hs = NULL;
	}
	return FALSE;
}

game_t *find_puzzle_solution(queue_t *q, hashset_ptr hs, int *local_c)
{
	int dir = 0;
	game_t *n = NULL;
	game_t *next = NULL;
	game_t *sol = NULL;

#ifdef DEBUG
	int depth = 0;
	printf("Finding Solution:\n");
#endif

	while (get_node_count(q)) {
		n = NULL;
		n = pop_first(q);
		if (!n) {
			fprintf(stderr, "queue empty??? find_puzzle_solution failed to pop a node.\n");
		}

#ifdef DEBUG
		count_all_nodes++;
		if (n->move_count > depth) {
			depth = n->move_count;
			printf("Evaluating %d move solutions\n", depth);
		}
#endif
		if (n->lowerb >= *local_c) {
			n = NULL;
			expell_queue_contents(q);
			continue;
		}
		if (is_solved((state_t *)n)) {
			if (n->move_count < *local_c) {
				*local_c = n->move_count;
				if (sol) {
					free_game(sol);
					sol = NULL;
				}
				sol = n;
				n = NULL;
			} 
			continue;
		}
			
		if (!next) {
			next = new_game();
			if (!next) {
				fprintf(stderr, "memory error while trying moves\n");
			}
		}
		copy_game(n, next);

		for (dir = 0; dir < 4; dir++) {
			if (dir == n->last_move_inv) {
				continue;
			}
			if (!game_move(next, dir, heuristic)) {
				continue;
			}
			if (hashset_insert(hs, next)) {
				insert_node(q, next);
				next = NULL;
			} 
			if (!next) {
				next = new_game();
			}
			if (!next) {
				fprintf(stderr, "memory error while trying moves\n");
			}
			copy_game(n, next);
		}
	}
	if (next) {
		free_game(next);
		next = NULL;
	}

	return sol;
}

void print_solution(game_t *start, game_t *solution)
{
	char *moves = malloc((solution->move_count + 1) * sizeof(char)); /*solution->moves; */
	int move_count = solution->move_count;
	int i;

	moves[solution->move_count] = '\0';

	get_solution_string_recursively(solution, moves);

	printf("Solution:\n\n");

	printf("Starting game:\n");
	print_game(start);
	printf("\n");
	for (i = 0; i < move_count; i++) {
		switch(moves[i]) {
			case 'U':
				printf("Move blanc space up:\n");
				game_move(start, UP, heuristic);
				break;
			case 'D':
				printf("Move blanc space down:\n");
				game_move(start, DOWN, heuristic);
				break;
			case 'L':
				printf("Move blanc space left:\n");
				game_move(start, LEFT, heuristic);
				break;
			case 'R':
				printf("Move blanc space right:\n");
				game_move(start, RIGHT, heuristic);
				break;
			default:
				printf("%d", moves[i]);
				printf("\n?\n\n");
		}
		print_game(start);
		printf("\n");
	}
	printf("Sequence of moves: %s\n", moves);
	free(moves);
}


int get_weight(void *n)
{
	game_t *game = (game_t *)n;
	return game->lowerb;
}

int cmp_n(void *n1, void *n2)
{
	game_t *a = n1;
	game_t *b = n2;

	return (a->lowerb - b->lowerb);
}


void free_n(void *n)
{
	game_t *game = n;

	free_game(game);
}


void print_g(void *n) 
{
	print_game((game_t *)n);
}
