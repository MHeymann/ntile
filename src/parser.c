#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <ctype.h>

#include "parser.h"

/*** Helper Function Prototypes ******************************************/

char next_non_digit(FILE *file);
char _get_file_size(FILE *file);

/*** Functions ***********************************************************/

/**
 * Get the number of integers in the file being read.
 *
 * @param filename A string, indicating the path to the file to 
 * open.  
 *
 * @return The number of integers in a single row in the file.  
 */
char get_file_size(char *filename)
{
	FILE *file = NULL;
	char N;

	file = fopen(filename, "r");
	if (!file) {
		fprintf(stderr, "problems with opening file\n");	
	}
	N = _get_file_size(file);	

	fclose(file);
	return N;
}

/**
 * Read the integers in the file and store it at at the buffer pointer.  
 * The buffer pointer gets allocated in this method.  It has to be free'd
 * later.  
 *
 * @param filename The path to the file to be read.  
 *
 * @param board a pointer to the array that must be allocated.  
 *
 * @param N The store the number of ints in a single row here.  
 */
void read_file(char *filename, char **board, char *N)
{
	int i = 0;
	char n_2;
	int entry = 0;
	int match_count = 0;
	char c;
	char *local_board;
	char *check_members;
	FILE *file;

	if (board == NULL) {
		fprintf(stderr, "Please give a valid int array pointer to read_file\n");
		return;
	}
	if (*board != NULL) {
		free(*board);
		*board = NULL;
	}

	file = fopen(filename, "r");

	if (!file) {
		fprintf(stderr, "Failed to open file\n");
		return;
	}

	*N = _get_file_size(file);
	n_2 = (*N) * (*N);
	local_board = malloc(n_2 * sizeof(char));
	check_members = malloc(n_2 * sizeof(char));

	if (!local_board) {
		fprintf(stderr, "Memory error:  could not allocate data for reading the game board. \n");
		exit(0);
	}
	if (!check_members) {
		fprintf(stderr, "Memory error:  could not allocate data for reading the game board. \n");
		exit(0);
	}

	for (i = 0; i < n_2; i++) {
		check_members[i] = 0;
	}
	
	i = 0;
	match_count = fscanf(file, "%d", &entry);
	while (match_count != EOF) {
		if (match_count == 1) {
			local_board[i] = entry;
			check_members[entry] = 1;
			i++;
		} else {
			match_count = fscanf(file, "%c", &c);
		}
		match_count = fscanf(file, "%d", &entry);
	}

	for (i = 0; i < n_2; i++) {
		if (!check_members[i]) {
			fprintf(stderr ,"Warning:  member %d not assigned in given input\n", i);
		}
	}

	*board = local_board;

	/* clean up */
	free(check_members);
	fclose(file);
}

/**
 * Write a file with the entries from board here.  
 *
 * @param filename The path to the file where it must be stored.  
 *
 * @param board The array of integers to store.  
 *
 * @param N the number of integers in a single row.  
 */
void write_file(char *filename, char *board, char N)
{
	FILE *file = NULL;
	int i, j;
	file = fopen(filename, "w");

	if (!file) {
		fprintf(stderr, "failure to open file %s for writing\n", filename);
		return;
	}

	if ((signed char)N < 0) {
		fprintf(stderr, "inadequite file size provided\n");
	}


	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			fprintf(file, "%d ", (int)board[i * N + j]);
		}		
	}
	fclose(file);
}

/*** Helper Functions ****************************************************/

/**
 * Advance the opened file to the next non digit char. 
 */
char next_non_digit(FILE *file) 
{
	char c;
	int match;

	match = fscanf(file, "%c", &c);
	while (isdigit(c)) {
		match = fscanf(file, "%c", &c);
		if (match == EOF) {
			break;
		}
	}

	return c;
}

/**
 * Read the file to get the number of integers stored in a single row
 * int the file.  
 *
 * @param file A pointer to the file being read.  
 *
 * @return The integer number of integers in a single row.  
 */
char _get_file_size(FILE *file)
{
	char N = 0;
	int rows = 0;
	int cols = 0;
	int match_count;
	char c;

	if (!file) {
		fprintf(stderr, "Invalid file stream for reading in get_file_size\n");
		return -1;
	}

	match_count = fscanf(file, "%c", &c);

	while (match_count != EOF) {
		if (isdigit(c)) {
			N++;
			c = next_non_digit(file);
			if ((signed char)c == EOF) {
				break;
			}	
		}
		if (c == '\n') {
			rows++;
			if (cols == 0) {
				cols = N;
			}
		}
		match_count = fscanf(file, "%c", &c);
	}

	rewind(file);
	if (cols != rows) {
		fprintf(stderr, "WARNING: error in inputfile format\n");
		return (char) sqrt((int)N);
	} else {
		return (int)rows;
	}
}


