#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#include "heuristic.h"
#include "walking/walking.h"
#include "walking/w_hashset.h"

#define ROW 0
#define COL 1

/*** Static global variable **********************************************/

static w_hashset_ptr whs = NULL;

/*** Helper Function Prototypes ******************************************/

int manhattan(state_t *state);

int walking_d(state_t *state);
unsigned long get_walking_id(state_t *state, int rc);


/*** Functions ***********************************************************/

/**
 * Get a heuristic value, using a function ass indicated by h.
 *
 * @param state The state for which to get a heuristic value.
 * @param h		In int that indicates which heuristic value to calculate.:w
 */
char get_heuristic_val(state_t *state, int h) 
{
	if (h == MANHATTAN) {
		return (char) manhattan(state);
	} else if(h == WALKING) {
		return (char) walking_d(state);
	} else {
		return 0;
	}
}

/** 
 * Initiate the hashset with values for using the walking distance
 * heuristic.
 * 
 * @param[in] N The dimension of the board being worked with. 
 *
 * return TRUE (1) if successfull and FALSE (0) if not.
 */
int init_walking(int N)
{
	if (!whs) {
		whs = get_walking_database(N);
		if (!whs) {
			return FALSE;
		} else {
			return TRUE;
		}
	} else {
		return TRUE;
	}
}

/**
 * Free The hashset being used for the walking distance heuristic. 
 */
void free_walking() 
{
	if (whs) {
		free_w_hashset(whs);
		whs = NULL;
	}
}

/*** Helper Functions ****************************************************/

/**
 * Calculate the manhattan number for a given state.
 * This comes down to a lowerbound for the minimum possible number of moves 
 * that can solve this board.  
 * 
 * @param state The state in question.  
 *
 * @return The manhattan value. 
 */
int manhattan(state_t *state)
{
	int r, c;
	int x;
	int sum = 0;
	int target_r, target_c;
	int diff_r, diff_c;
	for (r = 0; r < state->dimension; r++) {
		for (c = 0; c < state->dimension; c++) {
			x = (int)state->board[r * state->dimension + c]; 
			if (!x) {
				continue;
			} else {
				x--;
				target_r = x / state->dimension;
				target_c = x % state->dimension;
			}
			diff_r = r - target_r;
			diff_c = c - target_c;
			if (diff_r < 0) {
				diff_r *= -1;
			}
			if (diff_c < 0) {
				diff_c *= -1;
			}
			sum += (diff_r + diff_c);
		}
	}
	return sum;
}

/**
 * Get the walking distance heuristic.
 * 
 * @param state The state for which the walking distance
 *				must be calculated.
 */
int walking_d(state_t *state)
{
	int distance = 0;
	unsigned long id = -1;
	int value = 0;

	if (!whs) {
		printf("Error! didn't properly initialise walking properly!\n");
		init_walking(state->dimension);
		if (!whs) {
			printf("And initialisation unsuccessful\n");
		}
	}

	id = get_walking_id(state, ROW);
	if (w_hashset_lookup(whs, id, &value)) {
		distance += value;
	} else {
		printf("problem looking up id for row\n");
	}

	id = get_walking_id(state, COL);
	if (w_hashset_lookup(whs, id, &value)) {
		distance += value;
	} else {
		printf("problem looking up id for col\n");
	}
	/* relinquish lock */
	
	return distance;
}

/**
 * Calculate the lookup id for a given state.
 * @param state The state for which to get the id.
 * @param rc	An intteger that indicates whether the row id
 *				or the column id must be calculated.
 */
unsigned long get_walking_id(state_t *state, int rc)
{
	int r, c, x, n_2;
	int counts[36];
	unsigned long sum = 0;

	n_2 = state->dim_2;

	for (x = 0; x < n_2; x++) {
		counts[x] = 0;
	}

	for (r = 0; r < state->dimension; r++) {
		for (c = 0; c < state->dimension; c++) {
			x = state->board[r * state->dimension + c];
			if (!x) {
				continue;
			} else {
				x--;
				if (rc == ROW) {
					x /= state->dimension;
					counts[r * state->dimension + x]++;
				} else {
					x %= state->dimension;
					counts[c * state->dimension + x]++;
				}
			}
		}
	}


	for (r = 0; r < state->dimension; r++) {
		for (c = 0; c < state->dimension; c++) {
			x = r * state->dimension + c;
			sum += counts[n_2 - 1 - x] << (2 * x);
		}
	}
	
	return sum;
}

