#ifndef HEURISTIC_H
#define HEURISTIC_H

#define MANHATTAN	0
#define WALKING		1
#define FALSE		0
#define TRUE		1

#include "state.h"
#include "walking/w_state.h"

/**
 * Get a heuristic value, using a function ass indicated by h.
 *
 * @param state The state for which to get a heuristic value.
 * @param h		In int that indicates which heuristic value to calculate.
 */
char get_heuristic_val(state_t *state, int h);

/** 
 * Initiate the hashset with values for using the walking distance
 * heuristic.
 * 
 * @param[in] N The dimension of the board being worked with. 
 *
 * return TRUE (1) if successfull and FALSE (0) if not.
 */
int init_walking(int N);

/**
 * Free The hashset being used for the walking distance heuristic. 
 */
void free_walking();

#endif
