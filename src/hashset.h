/*
 * This lookuptable is specifically for the implementing a* with the aim
 * of solving an n-puzzle.
 *
 * It uses a generic hashtable, based on what Willem Bester made us use in
 * Computer Science 244.  
 *
 * “When the hurlyburly's done,
 * When the battle's lost and won.”
 *				― William Shakespeare, Macbeth
 */
#ifndef LOOKUPTABLE_H
#define LOOKUPTABLE_H

#include "game.h"

/*** Macros **************************************************************/

#define TRUE	1
#define SUCCESS	1
#define FALSE	0
#define FAIL	0
#define HT_ERROR 5

#define	LESS		1
#define WITH_EQUAL	2
#define	ALL 		3
#define	TRIVIAL		4

/*** Type Definitions ****************************************************/

typedef struct hashset *hashset_ptr;

/*** Function Prototypes *************************************************/

/**
 * Allocate the datastructures of a hashset and initialise the parameters.
 *
 * @param[in][out] ht a pointer to a pointer of the newly created hashset.
 */
void hashset_init_defaults(hashset_ptr *hs);


/**
 * Allocate the datastructures of a hashset and initialise the parameters.
 *
 * @param[in][out] ht		a pointer to a pointer of the newly created 
 *							hashset.
 * @param[in] init_delta	The power to raise two by to get the initial
 *							size of the underlying hashtable.
 * @param[in] delta_diff	The integer number to increment the delta
 *							value by when resizing the underlying 
 *							hashtable.
 * @param[in] cmp_choice	An integer parameter indicating the specific
 *							comparison function to use when comparing 
 *							games in the hashset.
 */
void hashset_init(hashset_ptr *hs, int init_delta, int delta_diff, 
		int cmp_choice);

/**
 * Insert a Agame instance into the hashset.
 *
 * @param[in] hs   A pointer to the hashset into which to insert.
 *
 * @param[in] game A pointer to the game instance to insert.
 *
 * @return SUCCESS(1) if the game was inserted successfully and
 * FAIL(0) if not.
 */
int hashset_insert(hashset_ptr hs, game_t *game);


/**
 * Force insertion of a game instance into the hashset.
 *
 * @param[in] hs   A pointer to the hashset into which to insert.
 *
 * @param[in] game A pointer to the game instance to insert.
 *
 * @return SUCCESS(1) if the game was inserted successfully and
 * FAIL(0) if not.
 */
int hashset_force_insert(hashset_ptr hs, game_t *game);

/**
 * Get a count of the total number of items in the hashset.
 *
 * @param[in] hs A pointer to the hashset for which to get a count.
 *
 * @return An integer of the total number of items in the hashset.
 */
int hashset_content_count(hashset_ptr hs);

/**
 *	Print the underlying hashtable of the hashset. 
 *
 *	@param[in] hs A pointer to the hashset to print.  
 */
void print_hashset(hashset_ptr hs);

/**
 * Free the values in the hashset and the hashset itself. 
 *
 * @param[in] hs The hashset to be free'd.  
 */
void free_hashset(hashset_ptr hs);

#endif
