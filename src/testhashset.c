#include <stdio.h>
#include <stdlib.h>

#include "heuristic.h"
#include "hashset.h"
#include "parser.h"

#define TRUE  1
#define FALSE 0

/*** Helper Function Prototypes ******************************************/

int assert_alloc(void *data, char *name); 

/*** Main Routine ********************************************************/

int main(int argc, char *argv[])
{
	char *buffer = NULL;
	char N = 0;
	game_t *game = NULL;
	game_t *cp_game = NULL;
	hashset_ptr hs = NULL;
	int ret_status = 0;

	if (argc != 2) {
		printf("Usage: %s <file>\n", argv[0]);
		printf("Where <file> is a file of a starting board\n");
		return 1;
	}

	read_file(argv[1], &buffer, &N);
	if (!assert_alloc(buffer, "buffer")) {
		ret_status = 2;
		goto CLEANUP;
	}
	
	game = new_game();
	if (!assert_alloc(game, "game")) {
		ret_status = 3;
		goto CLEANUP;
	}
	set_game(game, buffer, N, 0, "", MANHATTAN);	
	buffer = NULL;


	hashset_init(&hs, 3, 2, ALL);
	if (!hs) {
		printf("hashset_init() failed");
		free_game(game);
		ret_status = 4;
		goto CLEANUP;
	}

	ret_status = hashset_insert(hs, game);
	if (!ret_status) {
		printf("Failed to insert initial game\n");
	} else {
	}

	cp_game = new_game();
	if (!game) {
		fprintf(stderr, "error initializing cp_game\n");
	}
	copy_game(game, cp_game); 

	if (!hashset_insert(hs, cp_game)) {
		printf("Good! duplicate caught.\n");
	} else {
		printf("error, inserted the same game twice\n");
		print_hashset(hs);
	}

	game_move(cp_game, UP, 0);
	game_move(cp_game, LEFT, 0);

	if (hashset_insert(hs, cp_game)) {
		printf("Inserted\n");
		cp_game = NULL;
	} else {
		printf("error, failed to insert\n");
		print_hashset(hs);
	}	

	print_hashset(hs);

	ret_status = 0;
CLEANUP:
	if (cp_game) {
		free_game(cp_game);
	}
	if (buffer) {
		free(buffer);
	}
	if (hs) {
		free_hashset(hs);
	}

	return ret_status;
}

/*** Helper Functions ****************************************************/

int assert_alloc(void *data, char *name)
{
	if (!data) {
		printf("failed to initialize %s\n", name);
		return FALSE;
	} else {
		return TRUE;
	}
}
