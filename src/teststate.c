#include <stdio.h>
#include <stdlib.h>

#include "parser.h"
#include "state.h"

void test_all(state_t *state, int dir);

int main(int argc, char *argv[])
{
	char N = 0;
	char *buffer = NULL;
	state_t *state;
	state_t *copy;

	if (argc != 2) {
		printf("usage: %s <filename>\n", argv[0]);
		exit(1);
	}

	read_file(argv[1], &buffer, &N);

	state = new_state();
	copy = new_state();
	set_board(state, buffer, N);
	copy_state(state, copy);
	
	print_state(state);
	test_all(state, LEFT);
	test_all(state, UP);
	test_all(state, RIGHT);
	test_all(state, DOWN);

	printf("The copy of the original:\n");
	print_state(copy);

	free_state(state);
	free_state(copy);

	return 0;
}

void test_all(state_t *state, int dir) 
{
	printf("Moving the blanc space ");
	switch (dir) {
		case LEFT:
			printf("left\n");
			break;
		case RIGHT:
			printf("right\n");
			break;
		case UP:
			printf("up\n");
			break;
		case DOWN:
			printf("down\n");
			break;
		default:
			fprintf(stderr, "You called test_all with an invalid direction parameter\n");
	}
	make_move(state, dir);
	print_state(state);
	printf("\n");
}


