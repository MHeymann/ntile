#ifndef STATE_H
#define STATE_H

#define DOWN	0
#define LEFT	1
#define UP		2
#define RIGHT	3

/*** Macros **************************************************************/

#define SUCCESS 1
#define FAIL	0
#define TRUE	1
#define FALSE	0

/*** Type Definitions ****************************************************/

typedef struct state {
	char *board;
	char dimension;
	char dim_2;
	char blanc_row;
	char blanc_col;
} state_t;

/*** Function Prototypes *************************************************/

/**
 * allocate a new state_t structure.
 *
 * @return the newly allocated structure.  
 */
state_t *new_state();

/**
 * Set a given board of integers to the game state, and fill in 
 * the various fields of game state as needed.  
 *
 * @param state The state to be set to. 
 *
 * @param board The integers in the given game state.  
 *
 * @param N		The dimension of the board, ie the number of integers
 *				in a row or a column.  
 */
void set_board(state_t *state, char *board, char N);

/** 
 * Copy from one state to another.
 *
 * @param state The state to be copied
 *
 * @param copy	The state to be copied to.  
 */
void copy_state(state_t *state, state_t *copy);

/**
 * Free the datastructures in as state and the state itself.  
 *
 * @param state The state to be freed.  
 */
void free_state(state_t *state);

/**
 * Move the blanc space in a given direction.  
 *
 * @param state		The state which must be altered.  
 *
 * @param direction The direction in which the blanc space must move.
 *					UP, DOWN, LEFT or RIGHT as described in state.h
 *
 * @return SUCCESS (1) if the move is possible and FAIL (0) if not. 
 */
int make_move(state_t *state, int direction);

/**
 * Determine mathematically if a given state can be solved. 
 *
 * @param state The state in question.  
 *
 * @return TRUE (1) if it is solveable or FALSE (0) if not.  
 */
int is_solveable(state_t *state);

/**
 * Determine if a given state is currently solved.  
 *
 * @param state The state in question.  
 *
 * @return TRUE (1) if the state is in a solved state and 
 * FALSE (0) if not.  
 */
int is_solved(state_t *state);

/**
 * Print the state and all its accompanying values.  
 *
 * @param state The state in question.
 */
void print_state(state_t *state);


#endif
