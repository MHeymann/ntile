#include <stdio.h> 
#include <stdlib.h>

#include "state.h"
#include "heuristic.h"

/*** Helper Function Prototypes ******************************************/

int get_inverse_count(state_t *state);
int copy_state_over(state_t *original, state_t *copy, int n_2);
void print_board(state_t *state);

/*** Functions ***********************************************************/

/**
 * allocate a new state_t structure.
 *
 * @return the newly allocated structure.  
 */
state_t *new_state()
{
	state_t *state;
		
	/* allocate memory and initialise values */
	state = malloc(sizeof(state_t));
	state->board = NULL;
	state->dimension = 0;
	state->dim_2 = 0;
	state->blanc_row = -1;
	state->blanc_col = -1;
	
	return state;
}

/**
 * Set a given board of integers to the game state, and fill in 
 * the various fields of game state as needed.  
 *
 * @param state The state to be set to. 
 *
 * @param board The integers in the given game state.  
 *
 * @param N The dimension of the board, ie the number of integers
 * in a row or a column.  
 */
void set_board(state_t *state, char *board, char N)
{
	int i;
	char n_2;

	n_2 = N * N;
	for(i = 0; i < n_2; i++) {
		if (board[i] == '\0') {
			break;
		}
	}

	state->board = board;
	state->dimension = N;
	state->dim_2 = n_2;
	state->blanc_row = i / N;
	state->blanc_col = i % N;
}

/** 
 * copy from one state to another.
 *
 * @param state The state to be copied
 *
 * @param copy The state to be copied to.  
 */
void copy_state(state_t *state, state_t *copy)
{
	if (!state) {
		fprintf(stderr, "Please provide a valid pointer to the state to be copied\n");
	}
	if (!copy) {
		fprintf(stderr, "Please provide a valid pointer to where the state must be copied\n");
	}
	if (!copy->board) {
		copy->board = malloc(state->dim_2 * sizeof(char));
		if (!copy->board) {
			fprintf(stderr, "malloc error when copying states\n");
		}
	}
	if (!copy_state_over(state, copy, state->dim_2)) {
		fprintf(stderr, "Eish... malloc issues in copystate\n");
	}
}

/**
 * Free the datastructures in as state and the state itself.  
 *
 * @param state The state to be freed.  
 */
void free_state(state_t *state)
{
	if (!state) {
		return;
	}
	if (state->board) {
		free(state->board);
		state->board = NULL;
	}
	free(state);
}

/**
 * Move the blanc space in a given direction.  
 *
 * @param state The state which must be altered.  
 *
 * @param direction The direction in which the blanc space must move.
 * UP, DOWN, LEFT or RIGHT as described in state.h
 *
 * @return SUCCESS (1) if the move is possible and FAIL (0) if not. 
 */
int make_move(state_t *state, int direction)
{
	char temp;
	int new_row, new_col;
	char N = state->dimension;
	switch(direction) {
		case LEFT:
DEFAULT_LABEL:
			new_row = state->blanc_row;
			new_col = state->blanc_col - 1;
			if (new_col < 0) {
				return FAIL;
			}
			break;
		case RIGHT:
			new_row = state->blanc_row;
			new_col = state->blanc_col + 1;
			if (new_col >= N) {
				return FAIL;
			}
			break;
		case UP:
			new_row = state->blanc_row - 1;
			new_col = state->blanc_col;
			if (new_row < 0) {
				return FAIL;
			}
			break;
		case DOWN:
			new_row = state->blanc_row + 1;
			new_col = state->blanc_col;
			if (new_row >= N) {
				return FAIL;
			}
			break;
		default:
			fprintf(stderr, "Invalid direction provided to make_move in state.c\n");
			goto DEFAULT_LABEL;
	}

	temp = state->board[new_row * N + new_col];
	state->board[new_row * N + new_col] = 0;
	state->board[state->blanc_row * N + state->blanc_col] = temp;
	state->blanc_row = new_row;
	state->blanc_col = new_col;

	return SUCCESS;
}

/**
 * Determine mathematically if a given state can be solved. 
 *
 * @param state The state in question.  
 *
 * @return TRUE (1) if it is solveable or FALSE (0) if not.  
 */
int is_solveable(state_t *state)
{
	char N = state->dimension;
	int result;
	int inv = get_inverse_count(state);
	
	result = inv % 2;
	
#ifdef DEBUG
	printf("Inverse = %d\nDimension: %d\nBlanc row %d\n", inv, 
			state->dimension, state->blanc_row);
	printf("N - 1 - blanc_row %d\n", N - 1 - state->blanc_row);
#endif

	if (N % 2) {
		/* N is odd */	
		return (!result);
	} else {
		/* N is even */	
		if ((N - 1 - state->blanc_row) % 2) {
			/* blanc in odd row */
		return (result);
		} else {
			/* blanc in even row */
		return (!result);
		}
	}
}

/**
 * determine if a given state is currently solved.  
 *
 * @param state The state in question.  
 *
 * @return TRUE (1) if the state is in a solved state and 
 * FALSE (0) if not.  
 */
int is_solved(state_t *state)
{
	int i;

	/* start from the bottom up, since this is the last 
	 * ones to be solved */
	if (state->board[state->dim_2 - 1]) {
		return FALSE;
	}

	for (i = state->dim_2 - 2; i >= 0; i--) {
		if (state->board[i] != (char)(i + 1)) {
			return FALSE;
		}
	}

	return TRUE;
}

/**
 * print the state and all its accompanying values.  
 *
 * @param state The state in question.
 */
void print_state(state_t *state) {
	print_board(state);
#ifdef DEBUGS
	printf("Blanc is in row: %d\n", state->blanc_row);
	printf("Blanc is in col: %d\n", state->blanc_col);
	if (is_solveable(state)) {
		printf("This is a \x1b[1;34msolveable\x1b[0m state\n");
	} else {
		printf("This is \x1b[1;31mnot a solveable\x1b[0m state\n");
	}
	if (is_solved(state)) {
		printf("This is a \x1b[1;34msolved\x1b[0m state\n");
	} else {
		printf("This is \x1b[1;31mnot a solved\x1b[0m state\n");
	}
	printf("Manhattan: %d\n", get_heuristic_val(state, MANHATTAN));
	printf("Walking: %d\n", get_heuristic_val(state, WALKING));
#endif
}

/*** Helper Functions ****************************************************/

/**
 * Get the number of inverses on the board. 
 * This is used for determining if a state is solveable. 
 *
 * @param state The state for which this must be determined.  
 *
 * @return The number of inverses found. 
 */
int get_inverse_count(state_t *state)
{
	int i, j;
	int x;
	int inverse_count = 0;

	for (i = 0; i < state->dim_2; i++) {
		x = (int)state->board[i];
		if (!x) {
			continue;
		}
		for (j = i + 1; j < state->dim_2; j++) {
			if (state->board[j] == '\0') {
				/* do nothing */
			} else if (x > state->board[j]) {
				inverse_count++;
			}
		}
	}

#ifdef DEBUG
	printf("inverse: %d\n", inverse_count);
#endif
	return inverse_count;
}

/**
 * Copy The data of one state over to another.  
 * 
 * @param original The state to be copied.  
 *
 * @param copy The state to which must be copied.  
 *
 * @param n_2 The number of integers on the board in total.  
 *
 * @return SUCCESS (1) if all goes well and FAIL if not.  
 */
int copy_state_over(state_t *original, state_t *copy, int n_2) 
{
	int i;

	if (!original) {
		return FALSE;
	}
	if (!copy) {
		return FALSE;
	}
	if (!copy->board) {
		return FALSE;
	}

	copy->dimension = original->dimension;
	copy->dim_2 = original->dim_2;
	copy->blanc_row = original->blanc_row;
	copy->blanc_col = original->blanc_col;

	for (i = 0; i < n_2; i++) {
		copy->board[i] = original->board[i];
	}
	
	return TRUE;
}

/**
 * Print the game board with a nice border around it.  
 *
 * @param state The state for which te board must be printed.  
 */
void print_board(state_t *state)
{
	int i, j;
	int N = state->dimension;
	char line[20];

	printf("\x1b[1;34m/*");
	for (i = 0; i < N; i++) {
		printf("----");
	}
	printf("*\\\x1b[0m\n");
	for (i = 0; i < N; i++) {
		sprintf(line, "%d", (int)state->board[i * N]);
		printf("\x1b[1;34m|\x1b[0m %4s", line);
		for (j = 1; j < N; j++) {
			sprintf(line, "%d", (int)state->board[i * N + j]);
			printf(",%3s", line);
		}
		printf(" \x1b[1;34m|\x1b[0m\n");
	}
	printf("\x1b[1;34m\\*");
	for (i = 0; i < N; i++) {
		printf("----");
	}
	printf("*/\x1b[0m\n");
}


