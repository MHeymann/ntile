/*
 * This is is the final parallel version of the n-puzzle solver.
 * It was written using pseudo code found here:
 * 
 *		http://paginas.fe.up.pt/~apm/CPAR/docs/ParBandB.pdf
 *
 * This code with it's passing around of problems still proved
 * inefficient, so I then implemented another approach where
 * problem states are distributed and then each process
 * solves this using the serial approach.  
 *
 * I called the first approach the passer approach and the 
 * second the neighbour approach. 
 * 
 * Murray Heymann 
 * first semester of 2016
 * Computer Science 314
 * Stellenbosch University
 *
 * Young Simba: "And we'll always be together, right?"
 * Mufasa:		"Simba, let me tell you something my father told me. Look
 *				 at the stars. The great kings of the past look down on
 *				 us from those stars."
 * Young Simba: "Really?"
 * Mufasa:		"Yes. So whenever you feel alone, just remember that those
 *				 kings will always be there to guide you. And so will I."
 *									--The Lion King
 */

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <mpi.h>

#include "parser.h"
#include "game.h"
#include "queue.h"
#include "hashset.h"
#include "heuristic.h"
#include "token.h"

/*** Macro definitions ***************************************************/

#define STOP			1
#define CONTINUE		2

#define TERMINATE_TAG	1
#define TOKEN_TAG		2
#define PROBLEM_TAG		3

#define NEIGHBOUR		0
#define PASSER			1

#define LINE_SIZE		200

#ifdef DEBUG
#	define DBG_Print(N) printf("%s", N)
#else
#	define DBG_Print(N)
#endif

/*** Global static variables *********************************************/

static int hs_init_delta = 24;
static int hs_init_diff = 2;
static int approach = PASSER;
static int verbose = FALSE;

static double comm_interval = 0.05;
static double term_interval = 0.1;

static char heuristic = WALKING;
#ifdef DEBUG
static int count_all_nodes = 0;
#endif

/*** Helper Function Prototypes ******************************************/

/* general user interaction functions */
void print_usage(char *pname);
void get_args(int argc, char *argv[]);
void assert(void *p, const char *message);
/* general user interaction functions */

/* workhorses */
game_t *open_game(char *filename, char *N, int my_rank);
char *problem_passer(game_t *root, int *sol_move_count, int my_rank, int comm_sz);
char *problem_neighbour(game_t *root, int *sol_move_count, int my_rank, int comm_sz);
int init_q_hs(queue_t **q, hashset_ptr *hs);
int is_solution_state(game_t *n, int *local_c, int global_c, char **sol);
void expand_node(queue_t *q, hashset_ptr hs, game_t *n, int local_c);
void print_solution(game_t *start, int move_count, char *moves);

/* priority queue functions */
int cmp_n(void *n1, void *n2);
void free_n(void *n);
void print_g(void *n);
int get_weight(void *n);

/* MPI functions */
void packup_game(game_t *game, void *space);
int get_successor(int my_rank, int comm_sz);
int sort_mail_passer(queue_t *q, hashset_ptr hs, int N, token_t *token, 
		char *sol, int *local_c, int *global_c, int *my_colour,
		int my_rank, int comm_sz);
int sort_mail_neighbour(queue_t *q, token_t *token, 
		char **sol, int *local_c, int *global_c, int *my_colour,
		int my_rank, int comm_sz);

/*** Main Routine ********************************************************/

int main(int argc, char *argv[])
{
	game_t *start = NULL;
	int my_rank = -1;
	int comm_sz = -1;
	char N = 0;
	char *moves = NULL;
	int sol_move_count = -1;

	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &comm_sz);
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

	if (argc < 2) {
		print_usage(argv[0]);
		goto CLEANUP;
	}
	get_args(argc, argv);

    /* Load the game from a file */
	start = open_game(argv[1], &N, my_rank);
	if (!start) {
		if (my_rank == 0) {
			printf("Failed to load game.\n");
		}
		goto CLEANUP;
	}

	/* Get the solution sequence */
	if (approach == NEIGHBOUR) {
		DBG_Print("Using the neighbour approach\n");
		moves = problem_neighbour(start, &sol_move_count, my_rank, comm_sz);
	} else {
		DBG_Print("Using the problem passer approach\n");
		moves = problem_passer(start, &sol_move_count, my_rank, comm_sz);
	}

#ifdef DEBUG
	/* 
	 * TODO: Add statement to collect all the counts 
	 * MPI_Reduce(count_all_nodes);
	 */
#endif

	if (my_rank == 0) {
	    /* Print the solution step by step */
		print_solution(start, sol_move_count,  moves);
#ifdef DEBUG
		printf("Total nodes: %d\n", count_all_nodes);
#endif
	}
	
CLEANUP:
	/* Free data structures */
	if(verbose) {
		printf("Cleaning up");
	}
	if (start) {
		free_game(start);
		start = NULL;
	}
	if(verbose) {
		printf(".");
	}
	if (moves) {
		free(moves);
	}
	if(verbose) {
		printf(".");
	}
	free_walking();
	if(verbose) {
		printf(".\n");
	}

	/* no segfaults or early exits */
	if(verbose) {
		printf("exiting normally\n");
	}
	MPI_Finalize();
	return 0;
}

/*** Helper Functions ****************************************************/

void print_usage(char *pname)
{
	printf("\x1b[1;31mUsage:\x1b[0m %s <filename>\n", pname);
	printf("Where <filename> is the name of an input text file in the \n");
	printf("correct format.\n");
}

/** get the arguments from the command line and set the appropriate flags.
 *
 * @param argc The number of arguments passed.
 * @param argv An array of string to be processed
 */
void get_args(int argc, char *argv[])
{
	int i;
	int test;
	char *s;
	char *end = NULL;
	for (i = 2; i < argc; i++) {
		s = argv[i];
		if (strncmp(argv[i], "-heur=", 6) == 0) {
			if (strncmp((s + 6), "man", 3) == 0) {
				heuristic = MANHATTAN;
#ifdef DEBUG
				printf("Using Manhattan Distance as heuristic.\n");
#endif
			} else {
				heuristic = WALKING;
#ifdef DEBUG
				printf("Using Walking Distance as heuristic.\n");
#endif
			}
		} else if (strncmp(argv[i], "-delta_diff=", 12) == 0) {
			hs_init_diff = (int)strtol(s + 12, &end, 10);
			if (s+12 == end) {
				printf(
				"error: please give an integer value for -delta_diff\n");
				exit(1);
			} else {
#ifdef DEBUG
				printf("set hashtable's delta increment value to %d\n", 
						hs_init_diff);
#endif
			}
		} else if (strncmp(argv[i], "-delta_init=", 12) == 0) {
			hs_init_delta = (int)strtol(s + 12, &end, 10);
			if (s+12 == end) {
				printf(
				"error: please give an integer value for -delta_init\n");
				exit(1);
			} else {
#ifdef DEBUG
				printf("set hashtable's initial delta value to %d\n", 
						hs_init_delta);
#endif
			}
		} else if (strncmp(argv[i], "-c_interval=", 12) == 0) {
			test = sscanf(s, "-c_interval=%lf", &comm_interval);
			if (test != 1) {
				printf(
				"error: please give an double value for -c_interval\n");
				exit(1);
			} else {
#ifdef DEBUG
				printf("set comm_interval to %lf\n", comm_interval);
#endif
			}
		} else if (strncmp(argv[i], "-t_interval=", 12) == 0) {
			test = sscanf(s, "-t_interval=%lf", &term_interval);
			if (test != 1) {
				printf(
				"error: please give an integer value for -t_interval\n");
				exit(1);
			} else {
#ifdef DEBUG
				printf("set term_interval to %lf\n", term_interval);
#endif
			}
		} else if (strncmp(argv[i], "--neighbour", 11) == 0) {
			approach = NEIGHBOUR;
		} else if (strncmp(argv[i], "--passer", 8) == 0) {
			approach = PASSER;
		} else if (strncmp(argv[i], "--verbose", 9) == 0) {
			goto VERBOSE;
		} else if (strncmp(argv[i], "-v", 2) == 0) {
VERBOSE:
			verbose = TRUE;
		} else {
			printf("Could not recognize argument\n");
		}
	}

	MPI_Comm_rank(MPI_COMM_WORLD, &i);
	if (verbose) {
		if (i == 0) {
			if (approach == PASSER) {
				printf("using the passer approach\n");
			} else {
				printf("using the neighbour approach\n");
			}
			if (heuristic == WALKING) {
				printf("Using the Walking distance as heuristic\n");
			} else {
				printf("Using the Manhattan distance as heuristic\n");
			}
			printf("Initial delta value: %d\n", hs_init_delta);
			printf("Difference value: %d\n", hs_init_diff);
			printf("Communication interval: %2.3f\n", comm_interval);
			printf("Termination interval: %2.3f\n", term_interval);
		}
	}
}

/** 
 * A function that checks whether a pointer is not a null pointer.
 * If it is null, it aborts the program.
 *
 * @param p The pointer to be evaluated.
 * @param message A message to print if the pointer is null.
 */
void assert(void *p, const char *message)
{
	if (!p) {
		printf("error: %s\n", message);
		exit(2);
	}
}

/**
 * Load a game from a file and check solveability.
 *
 * @param[in] filename The path of the file to load.
 * @param[out] N	The a pointer to a char where the dimension of the
 *					board must be stored.
 * @return			A pointer to a new game instance with the loaded file's
 *					data set.
 */
game_t *open_game(char *filename, char *N, int my_rank)
{
	char *buffer = NULL;
	game_t *game = NULL;

	if (!filename || !N) {
		fprintf(stderr, "please provide valid pointers to open_game\n");
		return NULL;
	}

	game = new_game();
	if (!game) {
		fprintf(stderr, "Memory error\n");
		return NULL;
	}
	
	if (my_rank == 0) {
		read_file(filename, &buffer, N);
		if (!buffer) {
			printf("Please provide a valid input file. \n");
			free_game(game);
			return NULL;
		}
		MPI_Bcast(N , 1, MPI_CHAR, 0, MPI_COMM_WORLD);
		MPI_Bcast(buffer, (*N) * (*N), MPI_CHAR, 0, MPI_COMM_WORLD);
	} else {
		MPI_Bcast(N , 1, MPI_CHAR, 0, MPI_COMM_WORLD);
		buffer = malloc(*N * *N * sizeof(char));
		if (!buffer) {
			return NULL;
		}
		MPI_Bcast(buffer, (*N) * (*N), MPI_CHAR, 0, MPI_COMM_WORLD);
		
	}

	if (heuristic == WALKING) {
		init_walking(*N);
		DBG_Print("Walking initiated\n");
	}
	
	set_game(game, buffer, *N, 0, "", heuristic);
	if (is_solveable((state_t *)game)) {
		return game;
	} else {
		if (my_rank == 0) {
			printf("Unsolveable game loaded\n");
		}
		free_game(game);
		return NULL;
	}
}

/**
 * Get the solution to the puzzle.
 *
 * @param[in] root	A pointer to a game instance in the starting position
 *					that needs to be solved.
 * @param[out] sol_move_count A pointer to an integer, where the move count
 *					of the solution must be stored.
 * @return A string representation of the moves to be made to solve the puzzle.
 */
char *problem_passer(game_t *root, int *sol_move_count, int my_rank, int comm_sz)
{
	int my_colour;
	int local_c;
	int global_c;
	double last_comm = 0;
	double last_term = 0;
	double curr_time = 0;
	MPI_Status status;

	queue_t *q;
	hashset_ptr hs;
	token_t token;

	char line[LINE_SIZE];

#ifdef DEBUG
	int depth = 0;
#endif
	int stop;
	int i;
	game_t *n = NULL;
	game_t *next = NULL;
	game_t *temp = NULL;
	char *sol = NULL;

	sol = malloc(sizeof(char) * MAX_SOL_SIZE);
	if (!init_q_hs(&q, &hs)) {
		return NULL;
	}

	n = new_game();
	copy_game(root, n);
	if (hashset_insert(hs, n)) {
		insert_node(q, n);
		n = NULL;
	} else {
		printf("Hashset is being funny.\n");
		free_game(n);
		n = NULL;
		free_queue(q);
		q = NULL;
		free_hashset(hs);
		hs = NULL;
		return NULL;
	}
	
	if (my_rank == 0) {
		token.colour = WHITE;
		token.msg_count = 0;
		token.sol_move_count = INT_MAX;
		for (i = 0; i < MAX_SOL_SIZE; i++) {
			token.solution[i] = '\0';
		}
		MPI_Send(&token, sizeof(token_t), MPI_BYTE, 
				get_successor(my_rank, comm_sz), 
				TOKEN_TAG, MPI_COMM_WORLD);
	}

	local_c = INT_MAX;
	global_c = INT_MAX;

	while (TRUE) {
		n = NULL;
		n = pop_first(q);
		if (!n) {
			printf("This is weird. Queue should not initially be empty.\n");
			break;
		}
		if (is_solution_state(n, &local_c, global_c, &sol)) {
			sprintf(line, "%d found a solution, %d moves ++\n%s ", my_rank, n->move_count, sol);
			DBG_Print(line);
			n = NULL;
			expell_queue_contents(q);
			break;
		}
			
		expand_node(q, hs, n, local_c);
		if (get_node_count(q) >= comm_sz) {
			break;
		}
	}
	n = NULL;

	if (get_node_count(q)) {
		for (i = 0; i < my_rank; i++) {
			n = pop_first(q);
			n = NULL;
		}
		temp = pop_first(q);
		for (i = my_rank + 1; i < comm_sz; i++) {
			n = pop_first(q);
			n = NULL;
		}
		if (my_rank != 0) {
			expell_queue_contents(q);
		}
		insert_node(q, temp);
		temp = NULL;
	}


	last_comm = MPI_Wtime() + 0.99 * comm_interval;
	last_term = MPI_Wtime();

	my_colour = WHITE;

#ifdef DEBUG
	depth = 0;
	printf("Finding Solution:\n");
#endif
	goto JUMPIN;
	while (TRUE) {
		curr_time = MPI_Wtime();
		if ((curr_time - last_term) > term_interval) {
			MPI_Iprobe(MPI_ANY_SOURCE, TERMINATE_TAG, MPI_COMM_WORLD, &stop, &status);
			if (stop) {
				sprintf(line, "%d terminate\n", my_rank);
				DBG_Print(line);
				MPI_Recv(&stop, 1, MPI_INT, 0, TERMINATE_TAG, MPI_COMM_WORLD, &status);
				break;
			}
			last_term = MPI_Wtime();
		}
		
		if ((curr_time - last_comm) > comm_interval) {
			if (sort_mail_passer(q, hs, (int)root->state.dimension, &token, sol,
						&local_c, &global_c, &my_colour, 
						my_rank, comm_sz) == STOP) {
				sprintf(line, "%d terminate\n", my_rank);
				DBG_Print(line);
				break;
			}
			last_comm = MPI_Wtime();
		}
		if (get_node_count(q) == 0) {
			if (sort_mail_passer(q, hs, (int)root->state.dimension, &token, sol, 
						&local_c, &global_c, &my_colour, 
						my_rank, comm_sz) == STOP) {
				sprintf(line, "%d terminate\n", my_rank);
				break;
			}
			last_comm = MPI_Wtime();
		}
JUMPIN:
		n = NULL;
		n = pop_first(q);
		if (!n) {
			continue;
		}

#ifdef DEBUG
		if (n->move_count > depth) {
			depth = n->move_count;
			if (my_rank == 0) {
				printf("Evaluating %d move solutions\n", depth);
			}
		}
#endif
		if (n->lowerb >= local_c) {
			n = NULL;
			expell_queue_contents(q);
			continue;
		} 
		my_colour = BLACK;
		if (is_solution_state(n, &local_c, global_c, &sol)) {
			sprintf(line, "%d found a solution, %d moves ++\n%s ", my_rank, n->move_count, sol);
			DBG_Print(line);
			n = NULL;
			continue;
		}
		
		expand_node(q, hs, n, local_c);
	} /* end of While(TRUE) */
	if (next) {
		free_game(next);
		next = NULL;
	}
	if (q) {
		expell_queue_contents(q);
		free_queue(q);
	}
	if (hs) {
#ifdef DEBUG
		count_all_nodes += hashset_content_count(hs);
#endif
		free_hashset(hs);
	}

	*sol_move_count = global_c;
	return sol;
}

/**
 * Get the solution to the puzzle.
 *
 * @param[in] root	A pointer to a game instance in the starting position
 *					that needs to be solved.
 * @param[out] sol_move_count A pointer to an integer, where the move count
 *					of the solution must be stored.
 * @return A string representation of the moves to be made to solve the puzzle.
 */
char *problem_neighbour(game_t *root, int *sol_move_count, int my_rank, int comm_sz)
{
	int my_colour;
	int local_c;
	int global_c;
	char line[LINE_SIZE];

	queue_t *q;
	hashset_ptr hs;
	token_t token;

#ifdef DEBUG
	int depth = 0;
#endif
	int i;
	game_t *n = NULL;
	game_t *temp = NULL;
	char *sol = NULL;

	sol = malloc(sizeof(char) * MAX_SOL_SIZE);

	if (!init_q_hs(&q, &hs)) {
		printf("Error: Problems initiating queue or hashset.\n");
		return NULL;
	}

	n = new_game();
	assert(n, "Failed to assign game to initiate with\n");
	copy_game(root, n);
	if (hashset_insert(hs, n)) {
		insert_node(q, n);
		n = NULL;
	} else {
		printf("Hashset is being funny.\n");
		free_game(n);
		n = NULL;
		free_queue(q);
		q = NULL;
		free_hashset(hs);
		hs = NULL;
		return NULL;
	}
	if (my_rank == 0) {
		token.colour = WHITE;
		token.msg_count = 0;
		token.sol_move_count = INT_MAX;
		for (i = 0; i < MAX_SOL_SIZE; i++) {
			token.solution[i] = '\0';
		}
		MPI_Send(&token, sizeof(token_t), MPI_BYTE, 
				get_successor(my_rank, comm_sz), 
				TOKEN_TAG, MPI_COMM_WORLD);
	}

	local_c = INT_MAX;
	global_c = INT_MAX;
	/* Populate queue appropriately */
	while (TRUE) {
		n = NULL;
		n = pop_first(q);
		if (!n) {
			printf("This is weird. Queue should not initially be empty.\n");
			break;
		}
		if (is_solution_state(n, &local_c, global_c, &sol)) {
			sprintf(line, "%d found a solution, %d moves ++\n%s ", my_rank, n->move_count, sol);
			DBG_Print(line);
			n = NULL;
			expell_queue_contents(q);
			break;
		}
			
		expand_node(q, hs, n, local_c);
		if (get_node_count(q) >= comm_sz) {
			break;
		}
	}
	n = NULL;

	if (get_node_count(q)) {
		for (i = 0; i < my_rank; i++) {
			n = pop_first(q);
			n = NULL;
		}
		temp = pop_first(q);
		for (i = my_rank + 1; i < comm_sz; i++) {
			n = pop_first(q);
			n = NULL;
		}
		if (my_rank != 0) {
			expell_queue_contents(q);
		}
		insert_node(q, temp);
		temp = NULL;
	}

	my_colour = WHITE;

#ifdef DEBUG
	depth = 0;
	printf("Finding Solution:\n");
#endif
	goto JUMP_IN;
	while (TRUE) {
		if (sort_mail_neighbour(q, &token, &sol, &local_c, &global_c, 
					&my_colour, my_rank, comm_sz) == STOP) {
			sprintf(line, "%d terminate\n", my_rank);
			DBG_Print(line);
			break;
		}
JUMP_IN:
		
		n = NULL;
		n = pop_first(q);
		if (!n) {
			continue;
		}

#ifdef DEBUG
		if (n->move_count > depth) {
			depth = n->move_count;
			if (my_rank == 0) {
				printf("Evaluating %d move solutions\n", depth);
			}
		}
#endif
		if (n->lowerb >= local_c) {
			n = NULL;
			expell_queue_contents(q);
			continue;
		} 
		my_colour = BLACK;

		if (is_solution_state(n, &local_c, global_c, &sol)) {
			sprintf(line, "%d found a solution, %d moves ++\n%s ", my_rank, n->move_count, sol);
			DBG_Print(line);
			n = NULL;
			continue;
		}
			
		expand_node(q, hs, n, local_c);
	} /* end of While(TRUE) */

	if (q) {
		expell_queue_contents(q);
		free_queue(q);
	}
	if (hs) {
#ifdef DEBUG
		count_all_nodes += hashset_content_count(hs);
#endif
		free_hashset(hs);
	}

	*sol_move_count = local_c;
	return sol;
}

/**
 * Check if a given game is in the solution state, and if it is, check if 
 * it is better than the value stored at local_c; if it is, update the 
 * value of local_c and put the new solution at sol.
 *
 * @param[in] n The game to be evaluated
 * @param[in][out] local_c A pointer to the move count of the best 
 *				solution found so far.
 * @param[out] sol A pointer to the solution string of the best solution
 *				found so far.
 */
int is_solution_state(game_t *n, int *local_c, int global_c, char **sol)
{
	if (is_solved((state_t *)n)) {
		if (n->move_count < global_c) { 
			*local_c = n->move_count;
			if (*sol) {
				free(*sol);
				*sol = NULL;
			}
			*sol = malloc(sizeof(char) * (n->move_count + 1));
			assert(*sol, "Could not assign solution string.");
			get_solution_string_recursively(n, *sol);
		} 
		return TRUE;
	} else {
		return FALSE;
	}
}

/**
 * Take a given node and check which children node can be generated from it.
 * The resultant nodes are placed in the queue, if appropriate. 
 *
 * @param[in] q		The queue used in a*.
 * @param[in] hs	The hashset used to avoid duplicates.
 * @param[in] n		The game to be expanded.
 * @param[in] local_c The move count of the best solution found so far. 
 */
void expand_node(queue_t *q, hashset_ptr hs, game_t *n, int local_c)
{
	game_t *next = NULL;
	int dir;

	next = new_game();
	assert(next, "memory error while trying moves");
	copy_game(n, next);

	for (dir = 0; dir < 4; dir++) {
		if (dir == n->last_move_inv) {
			continue;
		}
		if (!game_move(next, dir, heuristic)) {
			continue;
		}
		if (next->lowerb >= local_c) {
			copy_game(n, next);
			continue;
		}
		if (hashset_insert(hs, next)) {
			insert_node(q, next);
			next = NULL;
		} 
		if (!next) {
			next = new_game();
		}
		assert(next, "memory error while trying moves");
		copy_game(n, next);
	}
	if (next) {
		free_game(next);
		next = NULL;
	}
}

/**
 * Initiate the queue and the hashset to be used by a*.
 *
 * @param[out] q	A pointer to a pointer to the queue to be initiated for
 *					use by a*.
 * @param[out] hs	A pointer to a pointer to the hashset to be used to 
 *					avoid duplicate states. 
 */
int init_q_hs(queue_t **q, hashset_ptr *hs)
{
	if (!q || !hs)  {
		fprintf(stderr, "please provide valid pointers to alloc_structures\n");
		return FALSE;
	}

	*q = NULL;
	*hs = NULL;

	init_queue(q, cmp_n, free_n);
	if (!(*q)) {
		fprintf(stderr, "Failed to initialise queue\n");
		goto INIT_CLEANUP;
	}

	hashset_init(hs, hs_init_delta, hs_init_diff, WITH_EQUAL);
	if (!(*hs)) {
		fprintf(stderr, "Failed to initialise hashset\n");
		goto INIT_CLEANUP;
	}

	return TRUE;
	
INIT_CLEANUP:
	if (*q) {
		free_queue(*q);
		*q = NULL;
	}
	if (*hs) {
		free_hashset(*hs);
		*hs = NULL;
	}

	return FALSE;
}

/**
 * Print the solution of the game move by move.
 * @param[in] start		The starting position of the game.
 * @param[in] move_count The nubmer of moves in the solution.
 * @param[in] moves		A string representation of the moves involved in
 *						the solution.
 */
void print_solution(game_t *start, int move_count, char *moves)
{
	int i;

	printf("Solution:\n\n");

	printf("Starting game:\n");
	print_game(start);
	printf("\n");
	for (i = 0; i < move_count; i++) {
		switch(moves[i]) {
			case 'U':
				printf("Move blanc space up:\n");
				game_move(start, UP, heuristic);
				break;
			case 'D':
				printf("Move blanc space down:\n");
				game_move(start, DOWN, heuristic);
				break;
			case 'L':
				printf("Move blanc space left:\n");
				game_move(start, LEFT, heuristic);
				break;
			case 'R':
				printf("Move blanc space right:\n");
				game_move(start, RIGHT, heuristic);
				break;
			default:
				printf("%d", moves[i]);
				printf("\n?\n\n");
		}
		print_game(start);
		printf("\n");
	}
	
	printf("Number of moves: \t%d\n", move_count);
	printf("Sequence of moves:\t%s\n", moves);
}

/**
 * Priority Queue Function: compare two data entries in the queue,
 * in this case the lower bound of two game states.
 *
 * @param[in] n1 The first of the two items to be compared.
 * @param[in] n2 The second of the two items to be compared.
 * @return	0 if the two have the same lower bound, a negative
 *			nubmer if n1 comes before n2 and a positive number
 *			otherwise.
 */
int cmp_n(void *n1, void *n2)
{
	game_t *a = n1;
	game_t *b = n2;

	return (a->lowerb - b->lowerb);
}

/**
 * Priority Queue Function: Free a data entry in the queue, in this
 * case a game instance.
 *
 * @param[in] n The data item to be free'd.
 */
void free_n(void *n)
{
	game_t *game = n;

	free_game(game);
}

/**
 * Pritority Queue Function: Print a data item in the queue, in this case
 * a game board and its accompanying values.
 */
void print_g(void *n) 
{
	print_game((game_t *)n);
}

/**
 * calculate the successor node to which the token and problems must be sent 
 */
int get_successor(int my_rank, int comm_sz)
{
	if (comm_sz == 1) {
		return 0;
	} else {
		return ((my_rank + 1) % comm_sz);
	}
}


/**
 * Sort the mail from other threads for the neighbour approach.
 * @return STOP if the problem has been solved globally and CONTINUE
 * otherwise. 
 */
int sort_mail_passer(queue_t *q, hashset_ptr hs, int N, token_t *token, 
		char *sol, int *local_c, int *global_c, int *my_colour,
		int my_rank, int comm_sz) 
{
	static int msg_count = 0;
	int n, i, successor;
	char *buffer;
	MPI_Status status;
	game_t *g = NULL;

	n = 0;
	MPI_Iprobe(MPI_ANY_SOURCE, TOKEN_TAG, MPI_COMM_WORLD, &n, &status);
	if (n) {
		for (i = 0; i < n; i++) {
			if (i == 1) {
				printf("how is it possible for tokens to pile up?\n");
			}
			MPI_Recv(token, sizeof(token_t), MPI_BYTE, 
					MPI_ANY_SOURCE, TOKEN_TAG, MPI_COMM_WORLD, &status);
		}
		if (*local_c < token->sol_move_count) {
			token->sol_move_count = *local_c;
			for (i = 0; i < *local_c; i++) {
				token->solution[i] = sol[i];
			}
			token->solution[i] = '\0';
		}
		if (get_node_count(q)) {
			g = q->head->data;
			if (token->sol_move_count < g->lowerb) {
				expell_queue_contents(q);
			}
			g = NULL;
		}
		*global_c = token->sol_move_count;
		if (token->sol_move_count < *local_c) {
			*local_c = token->sol_move_count;
			for (i = 0; i < token->sol_move_count; i++) {
				sol[i] = token->solution[i];
			}
			sol[i] = '\0';
		}

		if (my_rank == 0) {
			if (*my_colour == BLACK) {
				goto NOT_DONE;
			}
			if (token->colour == BLACK) {
				goto NOT_DONE;
			}
			if ((token->msg_count + msg_count) == 0) {
				for (i = 1; i < comm_sz; i++) {
					MPI_Send(&i, 1, MPI_INT, i, TERMINATE_TAG, MPI_COMM_WORLD);
				}
				return STOP;
			} else {
NOT_DONE:
				token->colour = WHITE;
				token->msg_count = 0;
			}
		} else {
			/* rank is not 0 */
			if (*my_colour == BLACK) {
				token->colour = BLACK;
			}
			token->msg_count = token->msg_count + msg_count;
		}
		MPI_Send(token, sizeof(token_t), MPI_BYTE, 
				get_successor(my_rank, comm_sz), 
				TOKEN_TAG, MPI_COMM_WORLD);
		*my_colour = WHITE;
	}
	
	n = 0;
	MPI_Iprobe(MPI_ANY_SOURCE, PROBLEM_TAG, MPI_COMM_WORLD, &n, &status);
	for (i = 0; i < n; i++) {
		g = NULL;
		g = new_game();
		assert(g, "new_game() failed while receiving problems");
		MPI_Recv(g, sizeof(game_t), MPI_BYTE, MPI_ANY_SOURCE, PROBLEM_TAG,
				MPI_COMM_WORLD, &status);
		buffer = malloc(sizeof(char) * N * N);
		assert(buffer, "Malloc failed while receiving problems");
		MPI_Recv(buffer, (sizeof(char) * N * N), MPI_BYTE, MPI_ANY_SOURCE,
				PROBLEM_TAG, MPI_COMM_WORLD, &status);
		g->state.board = buffer;
		buffer = NULL;
		buffer = malloc((g->move_count + 1) * sizeof(char));
		assert(buffer, "Malloc failed while receiving problems");
		MPI_Recv(buffer, (sizeof(char) * (g->move_count + 1)), MPI_BYTE, 
				MPI_ANY_SOURCE, PROBLEM_TAG, MPI_COMM_WORLD, &status);

		g->moves = buffer;
		buffer = NULL;
		g->parent = NULL;
		g->last_move_inv = -1;
		
		msg_count--;
		*my_colour = BLACK;
		if (g->lowerb < *global_c) {
			if (hashset_force_insert(hs, g)) {
				insert_node(q, g);
				g = NULL;
			} else {
				printf("rejected\n");
				free_game(g);
				g = NULL;
			}
		} else {
			free_game(g);
			g = NULL;
		}
	}
	if (get_node_count(q) > 1) {
		g = pop_first(q);
		assert(g, "Queue Error when sending problem");
		successor = get_successor(my_rank, comm_sz);
		MPI_Send(g, (sizeof(game_t)), MPI_BYTE, successor, PROBLEM_TAG,
				MPI_COMM_WORLD);
		MPI_Send(g->state.board, (sizeof(char) * N * N), MPI_BYTE, 
				successor, PROBLEM_TAG, MPI_COMM_WORLD);
		buffer = malloc(sizeof(char) * (g->move_count + 1));
		assert(buffer, "failed to make buffer when sending problem");
		get_solution_string_recursively(g, buffer);
		MPI_Send(buffer, (sizeof(char) * (g->move_count + 1)), MPI_BYTE, 
				successor, PROBLEM_TAG, MPI_COMM_WORLD);
		free(buffer);
		g = NULL;
		msg_count++;
		*my_colour = BLACK;
	}

	return CONTINUE;
}

/**
 * Sort the mail from other threads for the neighbour approach.
 * @return STOP if the problem has been solved globally and CONTINUE
 * otherwise. 
 */
int sort_mail_neighbour(queue_t *q, token_t *token, 
		char **sol, int *local_c, int *global_c, int *my_colour,
		int my_rank, int comm_sz) 
{
	int n = 0;
	int stop;
	int i = 0;
	game_t *g = NULL;
	MPI_Status status;

	MPI_Iprobe(MPI_ANY_SOURCE, TOKEN_TAG, MPI_COMM_WORLD, &n, &status);
	if (n) {
		for (i = 0; i < n; i++) {
			if (i == 1) {
				printf("how is it possible for tokens to pile up?\n");
			}
			MPI_Recv(token, sizeof(token_t), MPI_BYTE, 
					MPI_ANY_SOURCE, TOKEN_TAG, MPI_COMM_WORLD, &status);
		}
		if (*local_c < token->sol_move_count) {
			token->sol_move_count = *local_c;
			for (i = 0; i < *local_c; i++) {
				token->solution[i] = *(*sol + i);
			}
			token->solution[i] = '\0';
		}
		if (get_node_count(q)) {
			g = q->head->data;
			if (token->sol_move_count < g->lowerb) {
				expell_queue_contents(q);
			}
			g = NULL;
		}
		*global_c = token->sol_move_count;
		if (token->sol_move_count < *local_c) {
			*local_c = token->sol_move_count;
			for (i = 0; i < token->sol_move_count; i++) {
				*(*sol + i) = token->solution[i];
			}
			*(*sol + i) = '\0';
		}

		if (my_rank == 0) {
			if (*my_colour == BLACK) {
				goto NOT_DONE;
			}
			if (token->colour == WHITE) {
				for (i = 1; i < comm_sz; i++) {
					MPI_Send(&i, 1, MPI_INT, i, TERMINATE_TAG, MPI_COMM_WORLD);
				}
				return STOP;
			} else {
NOT_DONE:
				token->colour = WHITE;
				token->msg_count = 0;
			}
		} else {
			/* rank is not 0 */
			if (*my_colour == BLACK) {
				token->colour = BLACK;
			}
		}
		MPI_Send(token, sizeof(token_t), MPI_BYTE, 
				get_successor(my_rank, comm_sz), 
				TOKEN_TAG, MPI_COMM_WORLD);
		*my_colour = WHITE;
	}
	MPI_Iprobe(MPI_ANY_SOURCE, TERMINATE_TAG, MPI_COMM_WORLD, &stop, &status);
		if (stop) {
			MPI_Recv(&stop, 1, MPI_INT, 0, TERMINATE_TAG, MPI_COMM_WORLD, &status);
			return STOP;
		}
	
	return CONTINUE;
}
