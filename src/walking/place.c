#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#include "place.h"

/*** Functions ***********************************************************/

/**
 * Allocate memory for a new place.  
 *
 * @return The newly allocated place.
 */
place_t *new_place() 
{
	place_t *place = NULL;
	
	place = malloc(sizeof(place_t));
	if (!place) {
		printf("Memory stuffup! ###########################################\n");
		return NULL;
	}
	place->w_state.board = NULL; 
	place->w_state.dimension = 0; 
	place->w_state.blanc_row = -1; 
	place->w_state.blanc_col = -1; 
	place->move_count = 0;
	place->last_move_inv = -1;

	return place;
}

/**
 * Set the board to the place and initiate the fields. 
 *
 * @param board The integers that make up the place, represented as chars.  
 *
 * @param N The dimension of the board. 
 */
void set_place(place_t *place, char *board, int N, int move_count)
{
	w_set_board((w_state_t *)place, board, N);
	place->move_count = move_count;
	/* TODO: copy this in if available */
	place->last_move_inv = -1;
}

/**
 * Make a copy of the provided place.   
 *
 * @param original The place to be copied.
 * @param copy The place to be copied to.
 *
 */

void copy_place(place_t *original, place_t *copy)
{
	if (!original) {
		fprintf(stderr, "please provide a valid place pointer for original\n");
	}

	if (!copy) {
		fprintf(stderr, "please provide a valid place pointer for copy\n");
	}

	copy_w_state((w_state_t *)original, (w_state_t *)copy);
	copy->move_count = original->move_count;
	copy->last_move_inv = original->last_move_inv;
}

/**
 * Free a place and its underlying datastructures.  
 *
 * @param place The place to be freed.  
 */
void free_place(place_t *place)
{
	if(!place){
		return;
	}
	if (place->w_state.board) {
		free(place->w_state.board);
		place->w_state.board = NULL;
	}
	free(place);
	place = NULL;
}

/** Print a given place.  
 *
 * @param place The place to print. 
 */
void print_place(place_t *place)
{
	print_w_state((w_state_t *)place);
#ifdef DEBUGN
	printf("movecount: %d\n", place->move_count);
	printf("Last Move Inverse as int: %d\n", place->last_move_inv);
#endif
}

/**
 * Make a move on the blanc space in the place.  
 *
 * @param place The place in question.
 *
 * @param dir The directin to move the blanc space, 
 * UP, DOWN, LEFT or RIGHT as described int w_state.h
 * 
 * @return SUCCESS (1) if the move was carried out successfully
 * and FAIL (0) if not.  
 */
int place_move(place_t *place, int dir)
{
	if (!w_make_move((w_state_t *)place, dir)) {
		return FAIL;
	}
	
	switch (dir) {
		case UP:
		case DOWN:
			place->move_count++;
			break;
		case LEFT:
		case RIGHT:
			/* nothing */
			break;
		default:
			fprintf(stderr, "you gave in invalid direction to move in.  \n");
	}
	place->last_move_inv = (dir + 2) % 4;
	return SUCCESS;
}
