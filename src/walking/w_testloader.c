#include <stdio.h>
#include <stdlib.h>

#include "loader.h"

int main(int argc, char *argv[])
{
	int N = 0;
	int i, j;
	char *buffer = NULL;
	char line[10];

	if (argc != 2) {
		printf("usage: %s <dimension>\n", argv[0]);
		exit(1);
	}

	N = atoi(argv[1]);
	buffer = get_board(N);

	for (i = 0; i < N; i++) {
		sprintf(line, "%d", buffer[i * N]);
		printf("%4s", line);
		for (j = 1; j < N; j++) {
			sprintf(line, "%d", buffer[i * N + j]);
			printf(",%3s", line);
		}
		printf("\n");
	}

	free(buffer);
	return 0;
}
