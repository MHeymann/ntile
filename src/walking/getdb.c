#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#include "walking.h"
#include "w_hashset.h"
#include "../hashtable.h"

/*** Global static variables *********************************************/


/*** Helper Function Prototypes ******************************************/


/*** Main Routine ********************************************************/

int main(int argc, char *argv[])
{
	int N = 0;
	w_hashset_ptr w_hs = NULL;

	if (argc != 2){
		printf("wrong args\n");
		exit(1);
	}
	
	N = atoi(argv[1]);

	w_hs = generate_parameter_walking_database(N, 16, 1);

	if (!w_hs) {
		printf("Ai!\n");
	}

	print_w_hashset_entries(w_hs);
	/*
	*/
	free_w_hashset(w_hs);

	return 0;
}

/*** Helper Functions ****************************************************/


