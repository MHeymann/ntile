#include <stdio.h>
#include <stdlib.h>

#include "loader.h"
#include "place.h"

void test_all(place_t *place, int dir);

int main(int argc, char *argv[])
{
	int N = 0;
	char *buffer = NULL;
	place_t *place = NULL;
	place_t *copy = NULL;

	if (argc != 2) {
		printf("usage: %s <dimension>\n", argv[0]);
		exit(1);
	}

	N = atoi(argv[1]);
	buffer = get_board(N);

	place = new_place();
	set_place(place, buffer, N, 0);

	copy = new_place();
	copy_place(place, copy);
	
	print_place(place);
	test_all(place, LEFT);
	test_all(place, UP);
	test_all(place, RIGHT);
	test_all(place, DOWN);
	test_all(place, LEFT);
	test_all(place, UP);
	test_all(place, RIGHT);
	test_all(place, DOWN);
	test_all(place, LEFT);
	test_all(place, UP);
	test_all(place, RIGHT);


	printf("The copy of the original:\n");
	print_place(copy);

	free_place(place);
	free_place(copy);

	return 0;
}

void test_all(place_t *place, int dir)
{
	printf("Moving the blanc space ");
	switch (dir) {
		case LEFT:
			printf("left\n");
			break;
		case RIGHT:
			printf("right\n");
			break;
		case UP:
			printf("up\n");
			break;
		case DOWN:
			printf("down\n");
			break;
		default:
			fprintf(stderr, "You called test_all with an invalid direction parameter\n");
	}
	place_move(place, dir);
	print_place(place);
	printf("\n");
}


