#include <stdio.h> 
#include <stdlib.h>

#include "w_state.h"

/*** Helper Function Prototypes ******************************************/

int copy_w_state_over(w_state_t *original, w_state_t *copy, int n_2);
void w_print_board(w_state_t *w_state);

/*** Functions ***********************************************************/

/**
 * allocate a new w_state_t structure.
 *
 * @return the newly allocated structure.  
 */
w_state_t *new_w_state()
{
	w_state_t *w_state;
		
	w_state = malloc(sizeof(w_state_t));
	w_state->board = NULL;
	w_state->dimension = 0;
	w_state->blanc_row = -1;
	w_state->blanc_col = -1;
	
	return w_state;
}

/**
 * Set a given board of integers to the game w_state, and fill in 
 * the various fields of game w_state as needed.  
 *
 * @param w_state The w_state to be set to. 
 *
 * @param board The integers in the given game w_state, represented
 * as chars.  
 *
 * @param N The dimension of the board, ie the number of integers
 * in a row or a column.  
 */
void w_set_board(w_state_t *w_state, char *board, int N)
{
	int n_2, i;

	n_2 = N * N;
	for(i = 0; i < n_2; i++) {
		if (!board[i]) {
			break;
		}
	}

	if (i == n_2) {
		fprintf(stderr, "No blanc space found when setting the board!\n");
		exit(-1);
	}
	w_state->board = board;
	w_state->dimension = N;
	w_state->blanc_row = i / N;
	w_state->blanc_col = i % N;
}

/** 
 * copy from one w_state to another.
 *
 * @param w_state The w_state to be copied
 *
 * @param w_state_copy The w_state to be copied to.  
 */
void copy_w_state(w_state_t *w_state, w_state_t *w_state_copy)
{
	int n_2;

	if (!w_state) {
		fprintf(stderr, "Please provide a valid pointer to the w_state to be copied\n");
	}
	if (!w_state_copy) {
		fprintf(stderr, "Please provide a valid pointer to where the w_state must be copied\n");
	}
	n_2 = w_state->dimension * w_state->dimension;
	if (!w_state_copy->board) {
		w_state_copy->board = malloc(n_2 * sizeof(int));
	}
	if (!copy_w_state_over(w_state, w_state_copy, n_2)) {
		fprintf(stderr, "Eish... malloc issues in copy_w_state\n");
	}
}

/**
 * Free the datastructures in as w_state and the w_state itself.  
 *
 * @param w_state The w_state to be freed.  
 */
void free_w_state(w_state_t *w_state)
{
	if (!w_state) {
		return;
	}
	if (w_state->board) {
		free(w_state->board);
	}
	free(w_state);
}

/**
 * Move the blanc space in a given direction.  
 *
 * @param w_state The w_state which must be altered.  
 *
 * @param direction The direction in which the blanc space must move.
 * UP, DOWN, LEFT or RIGHT as described in w_state.h
 *
 * @return SUCCESS (1) if the move is possible and FAIL (0) if not. 
 */
int w_make_move(w_state_t *w_state, int direction)
{
	int temp;
	int new_row, new_col;
	int N = w_state->dimension;
	switch(direction) {
		case LEFT:
DEFAULT_LABEL:
			new_row = w_state->blanc_row;
			new_col = w_state->blanc_col - 1;
			if (new_col < 0) {
				return FAIL;
			}
			break;
		case RIGHT:
			new_row = w_state->blanc_row;
			new_col = w_state->blanc_col + 1;
			if (new_col >= N) {
				return FAIL;
			}
			break;
		case UP:
			new_row = w_state->blanc_row - 1;
			new_col = w_state->blanc_col;
			if (new_row < 0) {
				return FAIL;
			}
			break;
		case DOWN:
			new_row = w_state->blanc_row + 1;
			new_col = w_state->blanc_col;
			if (new_row >= N) {
				return FAIL;
			}
			break;
		default:
			fprintf(stderr, "Invalid direction provided to make_move in w_state.c\n");
			goto DEFAULT_LABEL;
	}

	temp = w_state->board[new_row * N + new_col];
	w_state->board[new_row * N + new_col] = 0;
	w_state->board[w_state->blanc_row * N + w_state->blanc_col] = temp;
	w_state->blanc_row = new_row;
	w_state->blanc_col = new_col;

	return SUCCESS;
}


/**
 * Calculate the id number for a given w_state.
 * creates an id number for this w_state that can be used in a hash set or
 * similar.  
 * 
 * @param w_state The w_state in question.  
 *
 * @return The id value. 
 */
unsigned long w_get_id(w_state_t *w_state)
{
	int r, c, x, n_2;
	/*
	int pow;
	*/
	unsigned long sum = 0;
	int *counts = malloc(w_state->dimension * w_state->dimension * sizeof(int));
	if (!counts) {
		fprintf(stderr, "Problems mallocing for counting\n");
	}
	n_2 = w_state->dimension * w_state->dimension;
	for (x = 0; x < n_2; x++) {
		counts[x] = 0;
	}
	for (r = 0; r < w_state->dimension; r++) {
		for (c = 0; c < w_state->dimension; c++) {
			x = w_state->board[r * w_state->dimension + c]; 
			if (!x) {
				continue;
			} else {
				x--;
				counts[r * w_state->dimension + x]++;
			}
		}
	}
	/*
	pow = 1;
	*/
	for (r = 0; r < w_state->dimension; r++) {
		for (c = 0; c < w_state->dimension; c++) {
			x = r * w_state->dimension + c;
			sum += counts[n_2 - 1 - x] << (2 * x);/* * pow; */
			/*
			pow *= 2;
			*/
		}
	}
	free(counts);

	return sum;
}

/**
 * print the w_state and all its accompanying values.  
 *
 * @param w_state The w_state in question.
 */
void print_w_state(w_state_t *w_state) {
	w_print_board(w_state);
#ifdef DEBUGS
	printf("Blanc is in row: %d\n", w_state->blanc_row);
	printf("Blanc is in col: %d\n", w_state->blanc_col);
	printf("id: %ld\n", w_get_id(w_state));
#endif
}

/*** Helper Functions ****************************************************/
/**
 * Copy The data of one w_state over to another.  
 * 
 * @param original The w_state to be copied.  
 *
 * @param copy The w_state to which must be copied.  
 *
 * @param n_2 The number of integers on the board in total.  
 *
 * @return SUCCESS (1) if all goes well and FAIL if not.  
 */
int copy_w_state_over(w_state_t *original, w_state_t *copy, int n_2) 
{
	int i;

	if (!original) {
		return FALSE;
	}
	if (!copy) {
		return FALSE;
	}
	if (!copy->board) {
		return FALSE;
	}

	copy->dimension = original->dimension;
	copy->blanc_row = original->blanc_row;
	copy->blanc_col = original->blanc_col;

	for (i = 0; i < n_2; i++) {
		copy->board[i] = original->board[i];
	}
	
	return TRUE;
}

/**
 * Print the game board with a nice border around it.  
 *
 * @param w_state The w_state for which te board must be printed.  
 */
void w_print_board(w_state_t *w_state)
{
	int i, j;
	int N = w_state->dimension;
	char line[20];

	printf("\x1b[1;34m/*");
	for (i = 0; i < N; i++) {
		printf("----");
	}
	printf("*\\\x1b[0m\n");
	for (i = 0; i < N; i++) {
		sprintf(line, "%d", w_state->board[i * N]);
		printf("\x1b[1;34m|\x1b[0m %4s", line);
		for (j = 1; j < N; j++) {
			sprintf(line, "%d", w_state->board[i * N + j]);
			printf(",%3s", line);
		}
		printf(" \x1b[1;34m|\x1b[0m\n");
	}
	printf("\x1b[1;34m\\*");
	for (i = 0; i < N; i++) {
		printf("----");
	}
	printf("*/\x1b[0m\n");
}


