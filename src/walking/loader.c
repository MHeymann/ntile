#include <stdio.h>
#include <stdlib.h>

#include "loader.h"

/*** Helper Function Prototypes ******************************************/

/*** Functions ***********************************************************/

/**
 * Generate a board that has a dimesion of N.
 * The caller is responsible for freeing the newly allocated goard.
 *
 * @param N The dimension of the new board.
 *
 * @return	A pointer to a char array, where the chars are treated as 
 *			integer numbers.
 */
char *get_board(int N) 
{
	int i = 0;
	int j = 0;
	char *board = NULL;
	
	if (N < 0) {
		printf("please give a valid argument N to get_board in loader.\n");
		return NULL;
	}

	board = malloc(N * N * sizeof(char));
	if (!board) {
		fprintf(stderr, "Memory error in loader\n");
		return NULL;
	}

	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			board[i * N + j] = (char)(i + 1);
		}
	}
	board[N * N - 1] = '\0';

	return board;
}

/*** Helper Functions ****************************************************/


