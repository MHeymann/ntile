#ifndef TERMINATION_TOKEN_H
#define TERMINATION_TOKEN_H

/*** Macros **************************************************************/

#define WHITE 1
#define BLACK 2
#define MAX_SOL_SIZE 101

/*** Token Struct Type Definition ****************************************/

typedef struct token {
	int colour;
	int msg_count;
	int sol_move_count;
	char solution[MAX_SOL_SIZE];
} token_t;

#endif
