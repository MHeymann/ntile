/*
 * This is a serial solver for the n-puzzle.  I first wrote this serial
 * version and optimized it, before using it as base for a parallel
 * version.
 *
 * Murray Heymann
 * 2016
 * Computer Science 314
 * Stellenbosch University
 *
 * "You have always been such a good friend to me,
 * Through the thunder and the rain,
 * And when you're feeling lost in the snows of New York,
 * Lift your heart and think of me;"
 *				― Chris de burgh, Snows of New York
 */

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <pthread.h>
#include <semaphore.h>

#include "parser.h"
#include "game.h"
#include "queue.h"
#include "hashset.h"
#include "heuristic.h"

#define LINE_SIZE 200
#define TRUE	1
#define FALSE	0

/*** Global static variables *********************************************/

static int hs_init_delta = 24;
static int hs_init_diff = 2;

static int verbose = FALSE;
static char heuristic = WALKING;
static int count_all_nodes = 0;
static long thread_count = 4;
static int use_hashset = TRUE;

static queue_t *q = NULL;
static hashset_ptr hs = NULL;
static sem_t hs_q_lock;

#ifdef DEBUG
static int depth = 0;
static sem_t depth_lock;
#endif

static char *global_solution = NULL;
static int g_solution_count = INT_MAX;
static sem_t sol_lock;

/*** Helper Function Prototypes ******************************************/

/* user interaction functions */
void print_usage(char *pname);
void get_args(int argc, char *argv[]);
void assert(void *p, const char *message);

/* workhorse functions */
int init_q_hs(queue_t **q, hashset_ptr *hs);
game_t *open_game(char *filename, char *N);
void *a_star(void *arg_p);
int is_solution_state(game_t *n);
void expand_node(game_t *n);
void print_solution(game_t *start);


/* priority queue functions */
int cmp_n(void *n1, void *n2);
void free_n(void *n);
void print_g(void *n);

/*** Debugging tools *****************************************************/

#ifdef DEBUG
#	define DBG_Print(A) printf("%s", A)
#else
#	define DBG_Print(A)
#endif

/*** Main Routine ********************************************************/

int main(int argc, char *argv[])
{
	game_t *start = NULL;
	game_t *n = NULL;
	char line[LINE_SIZE];
	char N = 0;
	long i = 0;
	pthread_t *thread_handles = NULL;
	
	if (argc < 2) {
		print_usage(argv[0]);
		goto CLEANUP;
	}
	get_args(argc, argv);

	sem_init(&hs_q_lock, 0, 1);
	sem_init(&sol_lock, 0, 1);
#ifdef DEBUG
	sem_init(&depth_lock, 0, 1);
#endif

	/* Load the game from a file */
	start = open_game(argv[1], &N);
	if (!start) {
		printf("Failed to load game\n");
		exit(1);
	}

	thread_handles = malloc(thread_count * sizeof(pthread_t));
	if (!thread_handles) {
		printf("Failed to allocate thread_handles\n");
		goto CLEANUP;
	}

	/* init queue and hashset */
	if (!init_q_hs(&q, &hs)) {
		printf("Error: failed to initialise queue and hashset.\n");
		global_solution = NULL;
		exit(3);
	}

	
	/* insert a copy of the starting board into the queue and hashset */
	n = new_game();
	assert(n, "Failed to assign game to put in queue initially.");
	copy_game(start, n);
	sem_wait(&hs_q_lock);
	if (hashset_insert(hs, n)) {
		insert_node(q, n);
		n = NULL;
	} else {
		printf("hashset is being funny\n");
		free_game(n);
		n = NULL;
		free_queue(q);
		q = NULL;
		free_hashset(hs);
		hs = NULL;
		global_solution = NULL;
		goto CLEANUP;
	}
	sem_post(&hs_q_lock);

	/* Launch Threads */
	for (i = 0; i < thread_count; i++) {
		pthread_create(&thread_handles[i], NULL, a_star, (void *) i);
	}

	/* Get the solution sequence */
	printf("Main thread waiting idle for threads to rejoin\n");

	/* Rejoin Threads */
	for (i = 0; i < thread_count; i++) {
		pthread_join(thread_handles[i], NULL);
		printf("Rejoined thread %ld\n", i);
	}

	assert(global_solution, "Failed to get the solution");

	/* Print the solution step by step */
	print_solution(start);

	count_all_nodes = hashset_content_count(hs);
	sprintf(line, "Total nodes: %d\n", count_all_nodes);
	DBG_Print(line);
	
CLEANUP:
	/* Free data structures */
	if (q) {
		expell_queue_contents(q);
		free_queue(q);
		q = NULL;
	}
	if (hs) {
		free_hashset(hs);
		hs = NULL;
	}
	if (start) {
		free_game(start);
		start = NULL;
	}
	if (global_solution) {
		free(global_solution);
		global_solution = NULL;
	}
	if (thread_handles) {
		free(thread_handles);
		thread_handles = NULL;
	}

	free_walking();

	sem_destroy(&hs_q_lock);
	sem_destroy(&sol_lock);

#ifdef DEBUG
	sem_destroy(&depth_lock);
#endif

	/* no segfaults or early exits */
	printf("exiting normally\n");
	return 0;
}

/*** Helper Functions ****************************************************/

void print_usage(char *pname)
{
	printf("usage: %s <filename> <options>\n", pname);
}

/** get the arguments from the command line and set the appropriate flags.
 *
 * @param argc The number of arguments passed.
 * @param argv An array of string to be processed
 */
void get_args(int argc, char *argv[])
{
	long i;
	char *end = NULL;
	char *s = NULL;

	for (i = 2; i < argc; i++) {
		s = argv[i];
		if (strncmp(argv[i], "-heur=", 6) == 0) {
			if (strncmp((s + 6), "man", 3) == 0) {
				heuristic = MANHATTAN;
			} else {
				heuristic = WALKING;
			}
		} else if (strncmp(argv[i], "--no_hash", 9) == 0) {
			use_hashset = FALSE;
		} else if (strncmp(argv[i], "-thread_count=", 14) == 0) {
			thread_count = (int)strtol(s + 14, &end, 10);
			if (s+12 == end) {
				printf(
		"error: please give an integer value for -the thread count you want\n");
				printf("Defaulting to 4 threads\n");
			}
		} else if (strncmp(argv[i], "--verbose", 9) == 0) {
			goto VERBOSE;
		} else if (strncmp(argv[i], "-v", 2) == 0) {
VERBOSE:
			verbose = TRUE;
		} else if (strncmp(argv[i], "-delta_init=", 12) == 0) {
			hs_init_delta = (int)strtol(s + 12, &end, 10);
			if (s+12 == end) {
				printf(
				"error: please give an integer value for -delta_init\n");
				printf("Defaulting to 24\n");
				hs_init_delta = 24;
			} else {
			}
		} else if (strncmp(argv[i], "-delta_diff=", 12) == 0) {
			hs_init_diff = (int)strtol(s + 12, &end, 10);
			if (s+12 == end) {
				printf(
				"error: please give an integer value for -delta_diff\n");
				exit(1);
			}
		} else {
#ifdef DEBUG
			printf("Could not recognize argument\n");
#endif
		}
	}
	
	if (verbose) {
		printf("running %s with %ld threads\n", argv[0], thread_count);
		if (heuristic == MANHATTAN) {
			printf("Using manhattan distance as heuristic function\n");
		} else if (heuristic == WALKING) {
			printf("Using walking distance as heuristic function\n");
		}
		if (use_hashset) {
			printf("Using hashset to trim search tree\n");
		} else {
			printf("\x1b[1;31mNOT\x1b[0m using hashset to trim search tree\n");
		}
		printf("Hashtable initial size: greatest prime smaller than 2^%d\n", 
				hs_init_delta);
		printf("Hashtable size increase roughly by 2^%d when resizeing\n", 
				hs_init_diff);
	}
}

/** 
 * A function that checks whether a pointer is not a null pointer.
 * If it is null, it aborts the program.
 *
 * @param p The pointer to be evaluated.
 * @param message A message to print if the pointer is null.
 */
void assert(void *p, const char *message)
{
	if (!p) {
		printf("%s\n", message);
		exit(2);
	}
}

/**
 * Load a game from a file and check solveability.
 *
 * @param[in] filename The path of the file to load.
 * @param[out] N	The a pointer to a char where the dimension of the
 *					board must be stored.
 * @return			A pointer to a new game instance with the loaded file's
 *					data set.
 */
game_t *open_game(char *filename, char *N)
{
	char *buffer = NULL;
	game_t *game = NULL;

	if (!filename || !N) {
		fprintf(stderr, "please provide valid pointers to open_game\n");
		return NULL;
	}

	game = new_game();
	if (!game) {
		fprintf(stderr, "Memory error\n");
		return NULL;
	}

	read_file(filename, &buffer, N);
	if (!buffer) {
		printf("Please provide a valid input file. \n");
		free_game(game);
		return NULL;
	}
	if (heuristic == WALKING) {
		init_walking(*N);
		DBG_Print("Initiated walking distance\n");
	}

	set_game(game, buffer, *N, 0, "", heuristic);
	if (is_solveable((state_t *)game)) {
		return game;
	} else {
		printf("Unsolveable game loaded\n");
		free_game(game);
		free_walking();
		return NULL;
	}
}

/**
 * Get the solution to the puzzle.
 *
 * @param[out] sol_move_count A pointer to an integer, where the move count
 *					of the solution must be stored.
 */
void *a_star(void *arg_p)
{
	game_t *n = NULL;
	long my_rank = (long)arg_p;
	printf("%ld finding solution\n", my_rank);

	/* Find the solution */
	while(TRUE) {
		n = NULL;

		sem_wait(&hs_q_lock);
		n = pop_first(q);
		sem_post(&hs_q_lock);

		if (!n) {
			if (global_solution) {
				return NULL;
			} else {
				continue;
			}
		}

#ifdef DEBUG
		sem_wait(&depth_lock);
		if (n->move_count > depth) {
			depth = n->move_count;
			printf("Evaluating %d move solutions\n", depth);
		}
		sem_post(&depth_lock);
#endif
		/* No better solutions are possible any more */
		/*
		if (n->lowerb >= g_solution_count) {
			n = NULL;

			sem_wait(&hs_q_lock);
			expell_queue_contents(q);
			sem_post(&hs_q_lock);

			break;
		}

		*/
		/* check if this node is in the solution state and handle accordingly */
		if (is_solution_state(n)) {
			n = NULL;
			sem_wait(&hs_q_lock);
			sem_wait(&sol_lock);
			if (q->node_count) {
				n = q->head->data;
				if (n->lowerb >= g_solution_count) {
					expell_queue_contents(q);
				}
			}
			sem_post(&sol_lock);
			sem_post(&hs_q_lock);
			return NULL;
		}
					
		/* spawn children nodes and put in the queue */
		expand_node(n);
	}
	return NULL;
}

/**
 * Check if a given game is in the solution state, and if it is, check if 
 * it is better than the value stored at local_c; if it is, update the 
 * value of local_c and put the new solution at sol.
 *
 * @param[in] n The game to be evaluated
 * @param[out] sol A pointer to the solution string of the best solution
 *				found so far.
 */
int is_solution_state(game_t *n)
{
	if (is_solved((state_t *)n)) {

		sem_wait(&sol_lock);
		if (n->move_count < g_solution_count) {
			g_solution_count = n->move_count; 
			if (global_solution) {
				free(global_solution);
				global_solution = NULL;
			}
			global_solution = malloc(sizeof(char) * (n->move_count + 1));
			assert(global_solution, "Could not assign solution string.");
			get_solution_string_recursively(n, global_solution);
		}
		sem_post(&sol_lock);

		return TRUE;
	} else {
		return FALSE;
	}
}

/**
 * Take a given node and check which children node can be generated from it.
 * The resultant nodes are placed in the queue, if appropriate. 
 *
 * @param[in] q		The queue used in a*.
 * @param[in] hs	The hashset used to avoid duplicates.
 * @param[in] n		The game to be expanded.
 */
void expand_node(game_t *n) 
{
	game_t *next = NULL;
	int dir;

	next = new_game();
	assert(next, "memory error while trying moves");
	copy_game(n, next);

	for (dir = 0; dir < 4; dir++) {
		if (dir == n->last_move_inv) {
			continue;
		}
		if (!game_move(next, dir, heuristic)) {
			continue;
		}
		sem_wait(&sol_lock);
		if (next->lowerb >= g_solution_count) {
			copy_game(n, next);

			sem_post(&sol_lock);
			continue;
		}
		sem_post(&sol_lock);

		sem_wait(&hs_q_lock);
		if (hashset_insert(hs, next)) {
			insert_node(q, next);
			next = NULL;
		} 
		sem_post(&hs_q_lock);

		if (!next) {
			next = new_game();
		}
		assert(next, "memory error while trying moves");
		copy_game(n, next);
	}
	if (next) {
		free_game(next);
		next = NULL;
	}
}

/**
 * Initiate the queue and the hashset to be used by a*.
 *
 * @param[out] q	A pointer to a pointer to the queue to be initiated for
 *					use by a*.
 * @param[out] hs	A pointer to a pointer to the hashset to be used to 
 *					avoid duplicate states. 
 */
int init_q_hs(queue_t **q, hashset_ptr *hs)
{
	if (!q || !hs) {
		fprintf(stderr, "please provide valid pointers to alloc_structures\n");
		return FALSE;
	}

	*q = NULL;
	*hs = NULL;

	/* initialise local structures */
	init_queue(q, cmp_n, free_n);
	if (!(*q)) {
		fprintf(stderr, "Failed to initialise queue\n");
		goto INIT_CLEANUP;
	}

	if (use_hashset) {
		hashset_init(hs, hs_init_delta, hs_init_diff, LESS);
	} else {
		hashset_init(hs, hs_init_delta, hs_init_diff, TRIVIAL);
	}
	if (!(*hs)) {
		fprintf(stderr, "Failed to initialise hashset\n");
		goto INIT_CLEANUP;
	}

	return TRUE;

INIT_CLEANUP:
	if (*q) {
		free_queue(*q);
		*q = NULL;
	}
	/* the next line should free the game popped above */
	if (*hs) {
		free_hashset(*hs);
		*hs = NULL;
	}

	return FALSE;
}

/**
 * Print the solution of the game move by move.
 * @param[in] start		The starting position of the game.
 * @param[in] move_count The nubmer of moves in the solution.
 * @param[in] moves		A string representation of the moves involved in
 *						the solution.
 */
void print_solution(game_t *start)
{
	int i;

	printf("Solution:\n\n");

	printf("Starting game:\n");
	print_game(start);
	printf("\n");
	for (i = 0; i < g_solution_count; i++) {
		switch(global_solution[i]) {
			case 'U':
				printf("Move blanc space up:\n");
				game_move(start, UP, heuristic);
				break;
			case 'D':
				printf("Move blanc space down:\n");
				game_move(start, DOWN, heuristic);
				break;
			case 'L':
				printf("Move blanc space left:\n");
				game_move(start, LEFT, heuristic);
				break;
			case 'R':
				printf("Move blanc space right:\n");
				game_move(start, RIGHT, heuristic);
				break;
			default:
				printf("%d", global_solution[i]);
				printf("\n?\n\n");
		}
		print_game(start);
		printf("\n");
	}

	printf("Number of moves: \t%d\n", g_solution_count);
	printf("Sequence of moves:\t%s\n", global_solution);
}

/**
 * Priority Queue Function: compare two data entries in the queue,
 * in this case the lower bound of two game states.
 *
 * @param[in] n1 The first of the two items to be compared.
 * @param[in] n2 The second of the two items to be compared.
 * @return	0 if the two have the same lower bound, a negative
 *			nubmer if n1 comes before n2 and a positive number
 *			otherwise.
 */
int cmp_n(void *n1, void *n2)
{
	return (((game_t *)(n1))->lowerb - ((game_t *)(n2))->lowerb);
}

/**
 * Priority Queue Function: Free a data entry in the queue, in this
 * case a game instance.
 *
 * @param[in] n The data item to be free'd.
 */
void free_n(void *n)
{
	free_game((game_t *)n);
}

/**
 * Pritority Queue Function: Print a data item in the queue, in this case
 * a game board and its accompanying values.
 */
void print_g(void *n) 
{
	print_game((game_t *)n);
}

