#ifndef PARSER_H
#define PARSER_H

/*** Function Prototypes *************************************************/

/**
 * Get the number of integers in the file being read.
 *
 * @param filename	A string, indicating the path to the file to 
 *					open.  
 *
 * @return The number of integers in a single row in the file.  
 */
char get_file_size(char *filename);

/**
 * Read the integers in the file and store it at at the buffer pointer.  
 * The buffer pointer gets allocated in this method.  It has to be free'd
 * later.  
 *
 * @param filename	The path to the file to be read.  
 * @param board		A pointer to the array that must be allocated.  
 * @param N			The store the number of ints in a single row here.  
 */
void read_file(char *filename, char **board, char *N);

/**
 * Write a file with the entries from board here.  
 *
 * @param filename	The path to the file where it must be stored.  
 * @param board		The array of integers to store.  
 * @param N			the number of integers in a single row.  
 */
void write_file(char *filename, char *board, char N);

#endif
