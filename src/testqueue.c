#include <stdio.h>
#include <stdlib.h>

#include "heuristic.h"
#include "parser.h"
#include "game.h"
#include "queue.h"

/*** Helper Function Prototypes ******************************************/

void test_all(game_t *game, int dir);
int cmp_g(void *g1, void *g2);
void free_g(void *n);
void print_g(void *n);
void do_some_moves_and_insert(queue_t *q, game_t *n, int no_of_moves, 
		int *directions);

/*** Main Routine ********************************************************/

int main(int argc, char *argv[])
{
	char N = 0;
	int i;
	int count;
	char *buffer = NULL;
	game_t *game1 = NULL;
	game_t *game2 = NULL;
	game_t *game3 = NULL;
	queue_t *q = NULL;
	int directions1[1] = {DOWN};
	int directions2[1] = {LEFT};
	int directions3[5] = {LEFT, LEFT, LEFT, LEFT, LEFT};
	int directions4[8] = {UP, UP, UP, UP, UP, LEFT, DOWN, RIGHT};
	int directions5[1] = {RIGHT};
	int directions6[1] = {UP};

	if (argc != 2) {
		printf("usage: %s <filename>\n", argv[0]);
		exit(1);
	}

	read_file(argv[1], &buffer, &N);

	game1 = new_game();
	game2 = new_game();
	set_game(game1, buffer, N, 0, "", MANHATTAN);
	copy_game(game1, game2);

	init_queue(&q, cmp_g, free_g);

	/* Try popping from a queue with nothing in it.  */
	if ((game3 = pop_first(q))) {
		printf("This is weird! %p\n", (void *)game3);
	} else {
		printf("Nice!  doesn't break upon poping from empty queue\n");
	}

	/* 
	 * Lets enter some stuff into the queue. 
	 * Entering 8 stuff into the queue.  Change the macro 
	 * for the initial size of the queue to 5 to make sure the 
	 * resizing works when the queue gets full. 
	 */
	printf("About to insert first entry\n");
	insert_node(q, game1);
	printf("Just inserted the following game:\n");
	print_game(game1);
	printf("\n");
	game1 = new_game();
	copy_game(game2, game1);

	do_some_moves_and_insert(q, game1, 1, directions1);

	game1 = new_game();
	copy_game(game2, game1);
	do_some_moves_and_insert(q, game1, 1, directions2);
	
	game1 = new_game();
	copy_game(game2, game1);
	do_some_moves_and_insert(q, game1, 5, directions3);

	game1 = new_game();
	copy_game(game2, game1);
	do_some_moves_and_insert(q, game1, 8, directions4);

	game1 = new_game();
	copy_game(game2, game1);
	do_some_moves_and_insert(q, game1, 1, directions2);

	game1 = new_game();
	copy_game(game2, game1);
	do_some_moves_and_insert(q, game1, 1, directions5);
	
	game1 = new_game();
	copy_game(game2, game1);
	do_some_moves_and_insert(q, game1, 1, directions6);

	/* 
	 * show the original to ensure that the original doesn't 
	 * accidentally get tampered with.  
	 */
	printf("The copy of the original:\n");
	print_game(game2);
	printf("\n");

	/* Keep game1 as a copy */
	game1 = new_game();
	copy_game(game2, game1);

	/* free game2 so can be popped into */
	free_game(game2);
	count = get_node_count(q);
	print_queue(q, print_g);
	/* test whether emptying the queue works */
	empty_queue(q);

	/* 
	 * now that it is empty, check that popping from the empty queue
	 * doesn't cause problems. 
	 */
	for (i = 0; i < count + 1; i++) {
		printf("%d of %d) popping and printing:\n", i + 1, count + 1);
		if ((game2 = pop_first(q))) {
			print_game(game2);
			printf("\n");
			free_game(game2);
		} else {
			printf("seems empty\n\n");
		}
	}

	/* Check whether one can still add stuff to the emptied queue */
	game2 = new_game();
	copy_game(game1, game2);
	insert_node(q, game1);

	game1 = new_game();
	copy_game(game2, game1);
	do_some_moves_and_insert(q, game1, 8, directions4);

	game1 = new_game();
	copy_game(game2, game1);
	do_some_moves_and_insert(q, game1, 1, directions2);
	
	game1 = new_game();
	copy_game(game2, game1);
	do_some_moves_and_insert(q, game1, 5, directions3);

	game1 = new_game();
	copy_game(game2, game1);
	do_some_moves_and_insert(q, game1, 1, directions6);

	/* print this queue now */
	print_queue(q, print_g);

	/* Free data structures */
	free_game(game2);
	free_queue(q);

	/* no segfaults or early exits */
	printf("exiting normally\n");
	return 0;
}

/*** Helper Functions ****************************************************/

void test_all(game_t *game, int dir)
{
	printf("Moving the blanc space ");
	switch (dir) {
		case LEFT:
			printf("left\n");
			break;
		case RIGHT:
			printf("right\n");
			break;
		case UP:
			printf("up\n");
			break;
		case DOWN:
			printf("down\n");
			break;
		default:
			fprintf(stderr, 
					"You called test_all with an invalid direction parameter\n");
	}
	if (!game_move(game, dir, 0)) {
		printf("Caught a none move\n");
	}
	/*
	print_game(game);
	*/
	printf("\n");
}

void do_some_moves_and_insert(queue_t *q, game_t *n, int no_of_moves, 
		int *directions)
{
	int i; 

	for (i = 0; i < no_of_moves; i++) {
		test_all(n, directions[i]);
	}
	insert_node(q, n);
	print_game(n);
	printf("\n");
}

int cmp_g(void *g1, void *g2)
{
	game_t *a = g1;
	game_t *b = g2;

	return (a->lowerb - b->lowerb);
}


void free_g(void *n)
{
	game_t *game = n;

	free_game(game);
}


void print_g(void *n) 
{
	print_game((game_t *)n);
}
