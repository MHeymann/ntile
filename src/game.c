#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#include "game.h"
#include "heuristic.h"

/*** Helper Function Prototypes ******************************************/

int get_linear_conflicts(game_t *game);

/*** Funcions ************************************************************/

/**
 * Allocate memory for a new game.  
 *
 * @return The newly allocated game.
 */
game_t *new_game() 
{
	game_t *game = NULL;
	
	/* alloc the struct */
	game = malloc(sizeof(game_t));
	if (!game) {
		printf("Memory stuffup! ####################################\n");
		return NULL;
	}
	/* initialize all values */
	game->state.board = NULL; 
	game->state.dimension = 0; 
	game->state.dim_2 = 0; 
	game->state.blanc_row = -1; 
	game->state.blanc_col = -1; 
	game->move_count = 0;
	game->lowerb = CHAR_MAX;
	game->moves = NULL; 
	game->current_buffer = 0; /*MOVEBUFFER;*/
	game->last_move_inv = -1;
	game->parent = NULL;

	return game;
}

/**
 * Set the board to the game and initiate the fields. 
 *
 * @param board The integers that make up the game.  
 *
 * @param N The dimension of the board. 
 *
 * @param move_count The integer number of moves made up until this point.
 *
 * @param moves Moves made up to this point, given if now parent is known or
 *				available.
 * @param heuristic An integer that indicates the heuristic that will be used on
 *				this game.
 */
void set_game(game_t *game, char *board, char N, int move_count, char *moves, int heuristic)
{
	int i;
	
	/* as the first member of game_t is an instance of state_t, it can 
	 * be treated as an instance of a state */
	set_board((state_t *)game, board, N);
	game->move_count = move_count;
	game->lowerb = lower_bound(game, heuristic);
	game->last_move_inv = -1;
	game->parent = NULL;

	/* If some prior moves are known to have been made, copy these in
	 * as a string provided */
	if (moves) {
		/* make sure there is enough space for the moves */
		if (game->current_buffer <= move_count) {
			if (game->moves) {
				free(game->moves);
				game->moves = NULL;
			}
			game->current_buffer = 
				((move_count / MOVEBUFFER) * MOVEBUFFER  + MOVEBUFFER);
			game->moves = 
				malloc((game->current_buffer + 1) * sizeof(char));
			if (!game->moves) {
				fprintf(stderr, "memory error in set_game\n");
				exit(5);
			}
		}
		/* copy over from the provided string */
		for (i = 0; i < move_count; i++) {
			game->moves[i] = moves[i];
		}
		/* terminate the string */
		game->moves[move_count] = '\0';
	}
}

/**
 * Copy from one game to another.  
 * The game to copy to must already be initialised 
 *
 * @param original The game to be copied.
 *
 * @param copy The game to which to copy.  
 */
void copy_game(game_t *original, game_t *copy)
{
	if (!original) {
		fprintf(stderr, "please provide a valid game pointer for original\n");
	}
	if (!copy) {
		fprintf(stderr, "please provide a valid game pointer for copy\n");
	}
	copy_state((state_t *)original, (state_t *)copy);
	copy->move_count = original->move_count;
	/*
	copy->lowerb = lower_bound(copy);
	*/
	copy->lowerb = original->lowerb;
	/* do not copy the moves over, as only the root needs to keep them */
	if (copy->moves) {
		free(copy->moves);
		copy->moves = NULL;
	}
	copy->current_buffer = 0;
	copy->last_move_inv = original->last_move_inv;
	copy->parent = original;
}

/**
 * Free a game and its underlying datastructures.  
 *
 * @param game The game to be freed.  
 */
void free_game(game_t *game)
{
	if(!game){
		return;
	}
	if (game->state.board) {
		free(game->state.board);
		game->state.board = NULL;
	}
	if (game->moves) {
		free(game->moves);
		game->moves = NULL;
	}
	game->parent = NULL;
	free(game);
	game = NULL;
}

/**
 * Calculate the lower bound of moves that a given game will be solved in.
 * This includes the moves made up until now. 
 *
 * @param game The game in question.  
 */
int lower_bound(game_t *game, char heuristic)
{
	return get_heuristic_val((state_t *)game, (int)heuristic) + game->move_count;
}

/** Print a given game.  
 *
 * @param game The game to print. 
 */
void print_game(game_t *game)
{
#ifdef DEBUGN
	char *buffer = NULL;
#endif

	print_state((state_t *)game);
#ifdef DEBUG
	printf("stored lower bound: %d\n", game->lowerb);
	printf("manhattan lower bound: %d\n", lower_bound(game, MANHATTAN));
#endif
#ifdef DEBUGWALK
	printf("walking distance lower bound: %d\n",
			lower_bound(game, WALKING));
#endif
#ifdef DEBUGN
	buffer = malloc((game->move_count + 1) * sizeof(char));
	buffer[game->move_count] = '\0';
	get_solution_string_recursively(game, buffer);
	printf("movecount: %d\n", game->move_count);
	printf("Move buffer: %d\n", game->current_buffer);
	printf("Moves so far: %s\n", buffer);
	printf("Last Move Inverse as int: %d\n", game->last_move_inv);
	printf("Parent: %p\n", (void *)game->parent);
#endif
}

/**
 * Make a move on the blanc space in the game.  
 *
 * @param game The game in question.
 *
 * @param dir The directin to move the blanc space, 
 * UP, DOWN, LEFT or RIGHT as described int state.h
 * 
 * @return SUCCESS (1) if the move was carried out successfully
 * and FAIL (0) if not.  
 */
int game_move(game_t *game, char dir, char heuristic)
{
	if (!make_move((state_t *)game, (int)dir)) {
		return FAIL;
	}
	game->move_count++;
	game->lowerb = lower_bound(game, heuristic);
	/* calculate and store the move made's inverse */
	game->last_move_inv = (dir + 2) % 4;
	return SUCCESS;
}

/** 
 * Function to make a string of the moves followed to a given point
 * recursively called on each of a nodes parent.  
 * The string is stored in the string called buffer.  The caller
 * is responsible to ensure that buffer has enough space.  
 *
 * @param[in]	game The current game position for which all prior
 * moves must be obtained.  
 *
 * @param[out]	buffer The string in which the result must be stored.  
 */
void get_solution_string_recursively(game_t *game, char *buffer)
{
	int i;
	char m;

	if (game->move_count <= 0) {
		if (buffer) {
			buffer[0] = '\0';
		}
		return;
	}


	if (game->parent) {
		get_solution_string_recursively(game->parent, buffer);
	} else {
		/* no parent.  iterate through the string of prior moves,
		 * if it exists
		 */
		if (game->moves) {
			for (i = 0; i < game->move_count; i++) {
				buffer[i] = game->moves[i];
#ifdef DEBUG
#endif
			}
		} else {
			/* If there are still prior moves, but no parent and no
			 * moves string, indicate this erro by filling the buffer
			 * string with '?' at those locations*/
			for (i = 0; i < game->move_count; i++) {
				buffer[i] = '?';
			}
		}
	}	
	i = game->move_count - 1;
	/* Use the stored value of the last move's inverse to
	 * determine what that move was and store it in the
	 * appropriate location in the buffer string */
	switch(game->last_move_inv) {
		case LEFT:
			m = 'R';
			break;
		case RIGHT:
			m = 'L';
			break;
		case UP:
			m = 'D';
			break;
		case DOWN:
			m = 'U';
			break;
		default:
			goto SKIP_PLACEMENT;
			break;
	}
	buffer[i] = m;
SKIP_PLACEMENT:
	buffer[i + 1] = '\0';

}

/*** Helper Functions ****************************************************/

int get_linear_conflicts(game_t *game)
{
	int N = game->state.dimension;
	int conflicts = 0;
	int diff;
	int r, i, j;

	for (r = 0; r < N; r++) {
		for (i = 0; i < N; i++) {
			if (game->state.board[r * N + i] == 0) {
				continue;
			}
			if (((game->state.board[r * N + i] - 1) / N) != r) {
				continue;
			}
			for (j = i + 1; j < N; j++) {
				if (((game->state.board[r * N + j] - 1) / N) != r) {
					continue;
				}
				diff = game->state.board[r * N + j] - game->state.board[r * N + i];
				if (diff < 0) {
					conflicts++;
				} else if (diff == 0) {
					printf("very weird for a linear conflict\n");
				}
			}
		}
	}

	return conflicts * 2;
}

