/*
 * This lookuptable is specifically for the implementing a* with the aim
 * of solving an n-puzzle.
 *
 * It uses a generic hashtable, based on what Willem Bester made us use in
 * Computer Science 244.  
 *
 * “When the hurlyburly's done,
 * When the battle's lost and won.”
 *				― William Shakespeare, Macbeth
 */
#include <stdio.h>
#include <stdlib.h>

#include "hashtable.h"
#include "hashset.h"
#include "game.h"

/*** Lookuptable struct Description **************************************/

typedef struct hashset {
	hashtable_p ht;
} hashset_t;

/*** Helper Function Prototypes ******************************************/

unsigned long hash_game(void *key, unsigned int size);
int cmp_game_eq(void *a, void *b);
int cmp_game_less(void *a, void *b);
int cmp_game_all(void *a, void *b);
int cmp_game_trivial(void *a, void *b);
void val2str(void *key, void *val, char *buffer);
void dud_free(void *);
void g_free(void *val);

/*** Functions ***********************************************************/

/**
 * Allocate the datastructures of a hashset and initialise the parameters.
 *
 * @param[in][out] ht a pointer to a pointer of the newly created hashset.
 */
void hashset_init_defaults(hashset_ptr *hs)
{
	hashset_init(hs, -1, -1, ALL);
}	

/**
 * Allocate the datastructures of a hashset and initialise the parameters.
 *
 * @param[in][out] ht		a pointer to a pointer of the newly created 
 *							hashset.
 * @param[in] init_delta	The power to raise two by to get the initial
 *							size of the underlying hashtable.
 * @param[in] delta_diff	The integer number to increment the delta
 *							value by when resizing the underlying 
 *							hashtable.
 * @param[in] cmp_choice	An integer parameter indicating the specific
 *							comparison function to use when comparing 
 *							games in the hashset.
 */
void hashset_init(hashset_ptr *hs, int init_delta, int delta_diff, 
		int cmp_choice)
{
	hashtable_p ht = NULL;
	hashset_ptr hset = NULL;

	hset = malloc(sizeof(hashset_t));
	if (!hset) {
		fprintf(stderr, "Error allocating memory for lookuptable.\n");
		*hs = NULL;
		return;
	}

	if (cmp_choice == WITH_EQUAL) {
		ht = ht_init(0.75f, init_delta, delta_diff, hash_game, cmp_game_eq);
	} else if (cmp_choice == LESS){
		ht = ht_init(0.75f, init_delta, delta_diff, hash_game, cmp_game_less);
	} else if (cmp_choice == TRIVIAL){
		ht = ht_init(0.75f, init_delta, delta_diff, hash_game, cmp_game_trivial);
	} else {
		ht = ht_init(0.75f, init_delta, delta_diff, hash_game, cmp_game_all);
	}
	if (!ht) {
		fprintf(stderr, "Error initializing hashtable.\n");
		free(hset);
		*hs = NULL;
		return;
	}

	hset->ht = ht;

	*hs = hset;
}

/**
 * Insert a Agame instance into the hashset.
 *
 * @param[in] hs A pointer to the hashset into which to insert.
 *
 * @param[in] game A pointer to the game instance to insert.
 *
 * @return SUCCESS(1) if the game was inserted successfully and
 * FAIL(0) if not.
 */
int hashset_insert(hashset_ptr hs, game_t *game)
{
	int insert_status;
	state_t *state = (state_t *) game;

	insert_status = ht_insert(hs->ht, (void *)state, (void *)game);
	if (insert_status) {
		switch (insert_status) {
			case 1:
				fprintf(stderr, 
						"Memory error when inserting into hashtable\n");
				break;
			case KEY_PRESENT_IN_TABLE:
				/*just means it is present.*/
#ifdef DEBUGHS
				fprintf(stdout, "already in queue\n");
#endif
				break;
			default:
				fprintf(stderr, 
						"This is weird in hashset_insert table's switch statement: %d\n", 
						insert_status);
				break;
		}
		return FAIL;
	} else {
		return SUCCESS;
	}
}

/**
 * Force insertion of a game instance into the hashset.
 *
 * @param[in] hs A pointer to the hashset into which to insert.
 *
 * @param[in] game A pointer to the game instance to insert.
 *
 * @return SUCCESS(1) if the game was inserted successfully and
 * FAIL(0) if not.
 */
int hashset_force_insert(hashset_ptr hs, game_t *game)
{
	int insert_status;
	state_t *state = (state_t *) game;

	insert_status = ht_force_insert(hs->ht, (void *)state, (void *)game);
	if (insert_status) {
		switch (insert_status) {
			case 1:
				fprintf(stderr, 
						"Memory error when inserting into hashtable\n");
				break;
			default:
				fprintf(stderr, 
						"This is weird in hashset_insert table's switch statement: %d\n", 
						insert_status);
				break;
		}
		return FAIL;
	} else {
		return SUCCESS;
	}
}

/**
 * Get a count of the total number of items in the hashset.
 *
 * @param[in] hs A pointer to the hashset for which to get a count.
 *
 * @return An integer of the total number of items in the hashset.
 */
int hashset_content_count(hashset_ptr hs)
{
	return ht_item_count(hs->ht);
}



/**
 *	Print the underlying hashtable of the hashset. 
 *
 *	@param[in] hs A pointer to the hashset to print.  
 */
void print_hashset(hashset_ptr hs)
{
	print_ht(hs->ht, val2str);	
}

/**
 * Free the values in the hashset and the hashset itself. 
 *
 * @param[in] hs The hashset to be free'd.  
 */
void free_hashset(hashset_ptr hs)
{
	ht_free(hs->ht, dud_free, g_free);
	hs->ht = NULL;
	free(hs);
}


/*** Helper Functions ****************************************************/

/**
 * Get a string representation of a key value pair, to be put in buffer.
 *
 * @param[in] key The unique key of the pair, used to hash with.   
 *
 * @param[in] val The value of the pair.
 *
 * @param[out] buffer The char pointer where the string representation 
 * of the pair will be put.  
 */
void val2str(void *key, void *val, char *buffer)
{
	state_t *state = (state_t *)key;
	game_t *game = (game_t *) val;
	sprintf(buffer, "[%p: blanc at (%d, %d,) moves %d]", (void *) game, state->blanc_row, 
			state->blanc_col, game->move_count);	
}

/**
 * A function wrapper to free values in the hashtable with.
 *
 * @param[in] val The value to be free'd
 */
void g_free(void *val) 
{
	game_t *game = val;
	free_game(game);
}

/** 
 * Just a dud function, as in our case, the key and the value are the
 * same thing.  
 * The stuff inside are simply to keep the warning flags off.  
 *
 * @param[in] key The key that doesn't need freeing. 
 */
void dud_free(void *key) 
{
	int *i = key;
	int j = *i;
	j++;
}

/**
 * A hash function to be used when placing games in the hashtable.  
 *
 * @param[in] key The key to be used to hash with.  
 *
 * @param[in] size The size of the table into which to put the 
 * key value pair.  
 *
 * @return The hash value as an unsigned long. 
 */
unsigned long hash_game(void *key, unsigned int size)
{
	state_t *state = (state_t *)key;
	int i;
	int n_2 = state->dimension * state->dimension;
	unsigned long hash = 0;

	for (i = 0; i < n_2; i++) {
		hash = (hash << 4) + state->board[i];
	}
	
	return (hash % size);
}

/**
 * A comparison function used to compare two games when trying 
 * to put a value into the hashtable. 
 *  @param[in] a A game pointer
 *
 *  @param[in] b A game pointer
 *
 *  @return An integer value, 0 if the two games are in an identical
 *  state and a has a lowerbound less than or equall to b's,
 *  less than 0 if a comes before b and more than 0 otherwise. 
 */
int cmp_game_eq(void *a, void *b)
{
	game_t *A = a;
	game_t *B = b;
	int n_2 = A->state.dimension * A->state.dimension;
	int i;

	if (n_2 != B->state.dimension * B->state.dimension) {
		printf("comparing games of unequal dimension\n");
	}

	for (i = 0; i < n_2; i++) {
		if (A->state.board[i] == B->state.board[i]) {
			continue;
		} else {
			return (int)(A->state.board[i] - B->state.board[i]);
		}
	}
	return ((A->lowerb - B->lowerb) <= 0);
}

/**
 * A comparison function used to compare two games when trying 
 * to put a value into the hashtable. 
 *  @param[in] a A game pointer
 *
 *  @param[in] b A game pointer
 *
 *  @return An integer value, 0 if the two games are in an identical
 *  state and a has a lower lowerbound stored, 
 *  less than 0 if a comes before b and more than 0 otherwise. 
 */
int cmp_game_less(void *a, void *b)
{
	game_t *A = a;
	game_t *B = b;
	int n_2 = A->state.dimension * A->state.dimension;
	int i;
	/*
	return 1;
	*/

	if (n_2 != B->state.dimension * B->state.dimension) {
		printf("comparing games of unequal dimension\n");
	}

	for (i = 0; i < n_2; i++) {
		if (A->state.board[i] == B->state.board[i]) {
			continue;
		} else {
			return (int)(A->state.board[i] - B->state.board[i]);
		}
	}
	/*
	printf("%d %d: %d\n", A->lowerb, B->lowerb, (A->lowerb - B->lowerb) < 0);
	*/
	return ((A->lowerb - B->lowerb) < 0);
}

/**
 * A comparison function used to compare two games when trying 
 * to put a value into the hashtable. 
 *  @param[in] a A game pointer
 *
 *  @param[in] b A game pointer
 *
 *  @return An integer value, 0 if the two games are in an identical
 *  state, less than 0 if a comes before b and more than 0 otherwise. 
 */
int cmp_game_all(void *a, void *b)
{
	game_t *A = a;
	game_t *B = b;
	int n_2 = A->state.dimension * A->state.dimension;
	int i;

	if (n_2 != B->state.dimension * B->state.dimension) {
		printf("comparing games of unequal dimension\n");
	}

	for (i = 0; i < n_2; i++) {
		if (A->state.board[i] == B->state.board[i]) {
			continue;
		} else {
			return (int)(A->state.board[i] - B->state.board[i]);
		}
	}
	return 0;
}

/**
 * A comparison function used to compare two games when trying 
 * to put a value into the hashtable. 
 *  @param[in] a A game pointer
 *
 *  @param[in] b A game pointer
 *
 *  @return An integer value, 0 if the two games are in an identical
 *  state, less than 0 if a comes before b and more than 0 otherwise. 
 */
int cmp_game_trivial(void *a, void *b)
{
	long A = (long)a;
	long B = (long)b;
	if (A-B) {
		return 1;	
	} else {
		return 0;
	}
}
