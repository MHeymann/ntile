/*
 * This is a serial solver for the n-puzzle.  I first wrote this serial
 * version and optimized it, before using it as base for a parallel
 * version.
 *
 * Murray Heymann
 * 2016
 * Computer Science 314
 * Stellenbosch University
 *
 * "You have always been such a good friend to me,
 * Through the thunder and the rain,
 * And when you're feeling lost in the snows of New York,
 * Lift your heart and think of me;"
 *				― Chris de burgh, Snows of New York
 */

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>

#include "parser.h"
#include "game.h"
#include "queue.h"
#include "hashset.h"
#include "heuristic.h"

#define LINE_SIZE 200

/*** Global static variables *********************************************/

static int hs_init_delta = 24;
static int hs_init_diff = 2;

static char heuristic = WALKING;
static int count_all_nodes = 0;

/*** Helper Function Prototypes ******************************************/

/* user interaction functions */
void print_usage(char *pname);
void get_args(int argc, char *argv[]);
void assert(void *p, const char *message);

/* workhorse functions */
game_t *open_game(char *filename, char *N);
char *a_star(game_t *root, int *sol_move_count);
int init_q_hs(queue_t **q, hashset_ptr *hs);
int is_solution_state(game_t *n, int *local_c, char **sol);
void expand_node(queue_t *q, hashset_ptr hs, game_t *n, int local_c);
void print_solution(game_t *start, int sol_move_count, char *solution);


/* priority queue functions */
int cmp_n(void *n1, void *n2);
void free_n(void *n);
void print_g(void *n);

/*** Debugging tools *****************************************************/

#ifdef DEBUG
#	define DBG_Print(A) printf("%s", A)
#else
#	define DBG_Print(A)
#endif

/*** Main Routine ********************************************************/

int main(int argc, char *argv[])
{
	int local_c;
	game_t *start = NULL;
	char line[LINE_SIZE];
	char *moves = NULL;
	char N = 0;
	
	if (argc < 2) {
		print_usage(argv[0]);
	}
	get_args(argc, argv);

	/* Load the game from a file */
	start = open_game(argv[1], &N);
	if (!start) {
		printf("Failed to load game\n");
		exit(1);
	}

	/* Get the solution sequence */
	moves = a_star(start, &local_c);
	assert(moves, "Failed to get the solution");

	/* Print the solution step by step */
	print_solution(start, local_c, moves);

	sprintf(line, "Total nodes: %d\n", count_all_nodes);
	DBG_Print(line);
	
	/* Free data structures */
	if (start) {
		free_game(start);
		start = NULL;
	}
	if (moves) {
		free(moves);
		moves = NULL;
	}
	free_walking();

	/* no segfaults or early exits */
	printf("exiting normally\n");
	return 0;
}

/*** Helper Functions ****************************************************/

void print_usage(char *pname)
{
	printf("usage: %s <filename> <options>\n", pname);
		exit(1);
}

/** get the arguments from the command line and set the appropriate flags.
 *
 * @param argc The number of arguments passed.
 * @param argv An array of string to be processed
 */
void get_args(int argc, char *argv[])
{
	int i;
	char *end = NULL;
	char *s = NULL;

	for (i = 2; i < argc; i++) {
		s = argv[i];
		if (strncmp(argv[i], "-heur=", 6) == 0) {
			if (strncmp((s + 6), "man", 3) == 0) {
				heuristic = MANHATTAN;
#ifdef DEBUG
				printf("Using Manhattan Distance as heuristic.\n");
#endif
			} else {
				heuristic = WALKING;
#ifdef DEBUG
				printf("Using Walking Distance as heuristic.\n");
#endif
			}
		} else if (strncmp(argv[i], "-delta_diff=", 12) == 0) {
			hs_init_diff = (int)strtol(s + 12, &end, 10);
			if (s+12 == end) {
				printf(
				"error: please give an integer value for -delta_diff\n");
				exit(1);
			} else {
#ifdef DEBUG
				printf("set hashtable's delta increment value to %d\n", 
						hs_init_diff);
#endif
			}
		} else if (strncmp(argv[i], "-delta_init=", 12) == 0) {
			hs_init_delta = (int)strtol(s + 12, &end, 10);
			if (s+12 == end) {
				printf(
				"error: please give an integer value for -delta_init\n");
				exit(1);
			} else {
#ifdef DEBUG
				printf("set hashtable's initial delta value to %d\n", 
						hs_init_delta);
#endif
			}
		} else {
#ifdef DEBUG
			printf("Could not recognize argument\n");
#endif
		}
	}


	/*
	for (i = 2; i < argc; i++) {
		s = argv[i];
		if (strncmp(argv[i], "-heur=", 6) == 0) {
			if (strncmp((s + 6), "man", 3) == 0) {
				heuristic = MANHATTAN;
			} else {
				heuristic = WALKING;
			}
		} else if (strncmp(argv[i], "-delta_diff=", 12) == 0) {
			hs_init_diff = atoi(s + 12);
		} else if (strncmp(argv[i], "-delta_init=", 12) == 0) {
			hs_init_delta = atoi(s + 12);
		}
	}
	*/

}

/** 
 * A function that checks whether a pointer is not a null pointer.
 * If it is null, it aborts the program.
 *
 * @param p The pointer to be evaluated.
 * @param message A message to print if the pointer is null.
 */
void assert(void *p, const char *message)
{
	if (!p) {
		printf("%s\n", message);
		exit(2);
	}
}

/**
 * Load a game from a file and check solveability.
 *
 * @param[in] filename The path of the file to load.
 * @param[out] N	The a pointer to a char where the dimension of the
 *					board must be stored.
 * @return			A pointer to a new game instance with the loaded file's
 *					data set.
 */
game_t *open_game(char *filename, char *N)
{
	char *buffer = NULL;
	game_t *game = NULL;

	if (!filename || !N) {
		fprintf(stderr, "please provide valid pointers to open_game\n");
		return NULL;
	}

	game = new_game();
	if (!game) {
		fprintf(stderr, "Memory error\n");
		return NULL;
	}

	read_file(filename, &buffer, N);
	if (!buffer) {
		printf("Please provide a valid input file. \n");
		free_game(game);
		return NULL;
	}
	if (heuristic == WALKING) {
		init_walking(*N);
		DBG_Print("Initiated walking distance\n");
	}

	set_game(game, buffer, *N, 0, "", heuristic);
	if (is_solveable((state_t *)game)) {
		return game;
	} else {
		printf("Unsolveable game loaded\n");
		free_game(game);
		free_walking();
		return NULL;
	}
}

/**
 * Get the solution to the puzzle.
 *
 * @param[in] root	A pointer to a game instance in the starting position
 *					that needs to be solved.
 * @param[out] sol_move_count A pointer to an integer, where the move count
 *					of the solution must be stored.
 */
char *a_star(game_t *root, int *sol_move_count)
{
	game_t *n = NULL;
	char *sol = NULL;

	int local_c = INT_MAX;
	queue_t *q = NULL;
	hashset_ptr hs = NULL;

#ifdef DEBUG
	int depth = 0;
#endif
	DBG_Print("Finding Solution:\n");

	/* init queue and hashset */
	if (!init_q_hs(&q, &hs)) {
		printf("Error: failed to initialise queue and hashset.\n");
		return NULL;
	}
	
	/* insert a copy of the starting board into the queue and hashset */
	n = new_game();
	assert(n, "Failed to assign game to put in queue initially.");
	copy_game(root, n);
	if (hashset_insert(hs, n)) {
		insert_node(q, n);
		n = NULL;
	} else {
		printf("hashset is being funny\n");
		free_game(n);
		n = NULL;
		free_queue(q);
		q = NULL;
		free_hashset(hs);
		hs = NULL;
		return NULL;
	}

	/* Find the solution */
	while(TRUE) {
		n = NULL;
		n = pop_first(q);
		assert(n, "queue empty??? a_star failed to pop a node.\n");

#ifdef DEBUG
		if (n->move_count > depth) {
			depth = n->move_count;
			printf("Evaluating %d move solutions\n", depth);
		}
#endif
		/* No better solutions are possible any more */
		if (n->lowerb >= local_c) {
			n = NULL;
			expell_queue_contents(q);
			break;
		}
		/* check if this node is in the solution state and handle accordingly */
		if (is_solution_state(n, &local_c, &sol)) {
			n = NULL;
			break;
		}
					
		/* spawn children nodes and put in the queue */
		expand_node(q, hs, n, local_c);
	}

	/* Clean up the local queue and hashset datastructures */
	/*
	*/
	if (q) {
		expell_queue_contents(q);
		free_queue(q);
	}
	if (hs) {
		count_all_nodes = hashset_content_count(hs);
		free_hashset(hs);
	}

	*sol_move_count = local_c;
	return sol;
}

/**
 * Check if a given game is in the solution state, and if it is, check if 
 * it is better than the value stored at local_c; if it is, update the 
 * value of local_c and put the new solution at sol.
 *
 * @param[in] n The game to be evaluated
 * @param[in][out] local_c A pointer to the move count of the best 
 *				solution found so far.
 * @param[out] sol A pointer to the solution string of the best solution
 *				found so far.
 */
int is_solution_state(game_t *n, int *local_c, char **sol)
{
	if (is_solved((state_t *)n)) {
		if (n->move_count < *local_c) {
			*local_c = n->move_count; 
			if (*sol) {
				free(*sol);
				*sol = NULL;
			}
			*sol = malloc(sizeof(char) * (n->move_count + 1));
			assert(*sol, "Could not assign solution string.");
			get_solution_string_recursively(n, *sol);
		}
		return TRUE;
	} else {
		return FALSE;
	}
}

/**
 * Take a given node and check which children node can be generated from it.
 * The resultant nodes are placed in the queue, if appropriate. 
 *
 * @param[in] q		The queue used in a*.
 * @param[in] hs	The hashset used to avoid duplicates.
 * @param[in] n		The game to be expanded.
 * @param[in] local_c The move count of the best solution found so far. 
 */
void expand_node(queue_t *q, hashset_ptr hs, game_t *n, int local_c) 
{
	game_t *next = NULL;
	int dir;

	next = new_game();
	assert(next, "memory error while trying moves");
	copy_game(n, next);

	for (dir = 0; dir < 4; dir++) {
		if (dir == n->last_move_inv) {
			continue;
		}
		if (!game_move(next, dir, heuristic)) {
			continue;
		}
		if (next->lowerb >= local_c) {
			copy_game(n, next);
			continue;
		}
		if (hashset_insert(hs, next)) {
			insert_node(q, next);
			next = NULL;
		} 
		if (!next) {
			next = new_game();
		}
		assert(next, "memory error while trying moves");
		copy_game(n, next);
	}
	if (next) {
		free_game(next);
		next = NULL;
	}
}

/**
 * Initiate the queue and the hashset to be used by a*.
 *
 * @param[out] q	A pointer to a pointer to the queue to be initiated for
 *					use by a*.
 * @param[out] hs	A pointer to a pointer to the hashset to be used to 
 *					avoid duplicate states. 
 */
int init_q_hs(queue_t **q, hashset_ptr *hs)
{
	if (!q || !hs) {
		fprintf(stderr, "please provide valid pointers to alloc_structures\n");
		return FALSE;
	}

	*q = NULL;
	*hs = NULL;

	/* initialise local structures */
	init_queue(q, cmp_n, free_n);
	if (!(*q)) {
		fprintf(stderr, "Failed to initialise queue\n");
		goto INIT_CLEANUP;
	}

	hashset_init(hs, hs_init_delta, hs_init_diff, ALL);
	if (!(*hs)) {
		fprintf(stderr, "Failed to initialise hashset\n");
		goto INIT_CLEANUP;
	}

	return TRUE;

INIT_CLEANUP:
	if (*q) {
		free_queue(*q);
		*q = NULL;
	}
	/* the next line should free the game popped above */
	if (*hs) {
		free_hashset(*hs);
		*hs = NULL;
	}

	return FALSE;
}

/**
 * Print the solution of the game move by move.
 * @param[in] start		The starting position of the game.
 * @param[in] move_count The nubmer of moves in the solution.
 * @param[in] moves		A string representation of the moves involved in
 *						the solution.
 */
void print_solution(game_t *start, int move_count, char *moves)
{
	int i;

	printf("Solution:\n\n");

	printf("Starting game:\n");
	print_game(start);
	printf("\n");
	for (i = 0; i < move_count; i++) {
		switch(moves[i]) {
			case 'U':
				printf("Move blanc space up:\n");
				game_move(start, UP, heuristic);
				break;
			case 'D':
				printf("Move blanc space down:\n");
				game_move(start, DOWN, heuristic);
				break;
			case 'L':
				printf("Move blanc space left:\n");
				game_move(start, LEFT, heuristic);
				break;
			case 'R':
				printf("Move blanc space right:\n");
				game_move(start, RIGHT, heuristic);
				break;
			default:
				printf("%d", moves[i]);
				printf("\n?\n\n");
		}
		print_game(start);
		printf("\n");
	}

	printf("Number of moves: \t%d\n", move_count);
	printf("Sequence of moves:\t%s\n", moves);
}

/**
 * Priority Queue Function: compare two data entries in the queue,
 * in this case the lower bound of two game states.
 *
 * @param[in] n1 The first of the two items to be compared.
 * @param[in] n2 The second of the two items to be compared.
 * @return	0 if the two have the same lower bound, a negative
 *			nubmer if n1 comes before n2 and a positive number
 *			otherwise.
 */
int cmp_n(void *n1, void *n2)
{
	return (((game_t *)(n1))->lowerb - ((game_t *)(n2))->lowerb);
}

/**
 * Priority Queue Function: Free a data entry in the queue, in this
 * case a game instance.
 *
 * @param[in] n The data item to be free'd.
 */
void free_n(void *n)
{
	free_game((game_t *)n);
}

/**
 * Pritority Queue Function: Print a data item in the queue, in this case
 * a game board and its accompanying values.
 */
void print_g(void *n) 
{
	print_game((game_t *)n);
}

