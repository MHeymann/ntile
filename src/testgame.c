#include <stdio.h>
#include <stdlib.h>

#include "heuristic.h"
#include "parser.h"
#include "game.h"
#if 0
	#include "state.h"
#endif

void test_all(game_t *game, int dir);

int main(int argc, char *argv[])
{
	char N = 0;
	char *buffer = NULL;
	game_t *game = NULL;
	game_t *copy = NULL;

	if (argc != 2) {
		printf("usage: %s <filename>\n", argv[0]);
		exit(1);
	}

	read_file(argv[1], &buffer, &N);

	game = new_game();
	copy = new_game();
	set_game(game, buffer, N, 0, "", MANHATTAN);
	copy_game(game, copy);
	
	print_game(game);
	test_all(game, LEFT);
	test_all(game, UP);
	test_all(game, RIGHT);
	test_all(game, DOWN);
	test_all(game, LEFT);
	test_all(game, UP);
	test_all(game, RIGHT);
	test_all(game, DOWN);
	test_all(game, LEFT);
	test_all(game, UP);
	test_all(game, RIGHT);
	test_all(game, DOWN);


	printf("The copy of the original:\n");
	print_game(copy);

	free_game(game);
	free_game(copy);

	return 0;
}

void test_all(game_t *game, int dir)
{
	printf("Moving the blanc space ");
	switch (dir) {
		case LEFT:
			printf("left\n");
			break;
		case RIGHT:
			printf("right\n");
			break;
		case UP:
			printf("up\n");
			break;
		case DOWN:
			printf("down\n");
			break;
		default:
			fprintf(stderr, "You called test_all with an invalid direction parameter\n");
	}
	game_move(game, dir, 0);
	print_game(game);
	printf("\n");
}


