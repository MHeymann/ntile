#include <stdio.h>
#include <stdlib.h>

#include "parser.h"

int main(int argc, char *argv[])
{
	char N = 0;
	int i, j;
	char *buffer = NULL;
	char line[10];

	if (argc != 2) {
		printf("usage: %s <filename>\n", argv[0]);
		exit(1);
	}

	read_file(argv[1], &buffer, &N);

	for (i = 0; i < N; i++) {
		sprintf(line, "%d", (int)buffer[i * N]);
		printf("%4s", line);
		for (j = 1; j < N; j++) {
			sprintf(line, "%d", (int)buffer[i * N + j]);
			printf(",%3s", line);
		}
		printf("\n");
	}

	free(buffer);
	return 0;
}
