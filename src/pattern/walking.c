#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#include "walking.h"
#include "loader.h"
#include "place.h"
#include "../queue.h"
#include "../hashtable.h"
#include "w_hashset.h"

#define DEFAULT_DELTA 17
#define DEFAULT_DIFF  2

/*** Global static variables *********************************************/

#ifdef DEBUG
static int count_all_nodes = 0;
#endif

/*** Helper Function Prototypes ******************************************/

w_hashset_ptr create_tree(int N, int delta, int diff);
int init_q_w_hs(queue_t **q, w_hashset_ptr *w_hs, int dimension, 
		int delta, int diff);
void build_database(queue_t *q, w_hashset_ptr w_hs);
w_hashset_ptr read_database(char *fname, int delta, int diff);
void move_spread_insert(queue_t *q, w_hashset_ptr w_hs, place_t *node, 
		int dir);

/* queue functions */
int cmp_wn(void *n1, void *n2);
void free_wn(void *n);
void print_wn(void *n);

/*** Functions ***********************************************************/

/** 
 * Generate a hashset containing a database of walking distances for
 * given state paterns.  
 *
 * @param N The dimension for which the database must be generated.
 *
 * @return a w_hashset pointer for the set containing the database.
 */
w_hashset_ptr get_walking_database(int N)
{
	if (N < 1) {
		printf("invalid argument for generating database: %d\n", N);
		return NULL;
	}
	return get_parameter_walking_database(N, DEFAULT_DELTA, DEFAULT_DIFF);
}

/** 
 * Generate a hashset containing a database of walking distances for
 * given state paterns, with given hashtable parameters
 *
 * @param N		The dimension for which the database must be generated.
 * @param delta	The initial delta into the unverlying hashtable.
 * @param diff	The increment by which to increase the hashtable when
 *				resizing.
 *
 * @return a w_hashset pointer for the set containing the database.
 */
w_hashset_ptr get_parameter_walking_database(int N, int delta, int diff)
{
	w_hashset_ptr w_hs = NULL;
	
	if (N < 1) {
		printf("invalid argument for generating database: %d\n", N);
		return NULL;
	}

	if (N == 4) {
		w_hs = read_database("4walk.db", delta, diff);
	} else {
		w_hs = create_tree(N, delta, diff);
	}
	
	return w_hs;
}

w_hashset_ptr generate_parameter_walking_database(int N, int delta, 
		int diff)
{
	w_hashset_ptr w_hs = NULL;
	
	if (N < 1) {
		printf("invalid argument for generating database: %d\n", N);
		return NULL;
	}

	w_hs = create_tree(N, delta, diff);
	
	return w_hs;
}


/*** Helper Functions ****************************************************/

w_hashset_ptr create_tree(int N, int delta, int diff)
{
	queue_t *q = NULL;
	w_hashset_ptr w_hs = NULL;

	if (!init_q_w_hs(&q, &w_hs, N, delta, diff)) {
		fprintf(stderr, 
				"Something went wrong allocating the local structures.\n");
		goto CLEANUP_CREATE_TREE;
	}

	build_database(q, w_hs);

#ifdef DEBUG
	printf("Total nodes: %d\n", count_all_nodes);
#endif


CLEANUP_CREATE_TREE:
	/* Free data structures */
	if (q) {
		free_queue(q);
	}

	return w_hs;
}

/**
 * Initiate the q and the w_hashset for use in generating the database. 
 *
 * @param[out] q		A double queue pointer to where the queue must be 
 *						initialised. 
 * @param[out] w_hs		A double w_hashset pointer to where the w_hashset
 *						must be initialised.
 * @param[in] dimension	The dimension of the board for which the walking
 *						distance database must be generated. 
 */
int init_q_w_hs(queue_t **q, w_hashset_ptr *w_hs, int dimension, 
		int delta, int diff)
{
	char *buffer = NULL;
	place_t *place = NULL;

	if (!q || !w_hs || (dimension < 1)) {
		fprintf(stderr, 
				"please provide valid parameters to alloc_structures\n");
		return FAIL;
	}

	/* initialise local structures */
	place = new_place();
	if (!place) {
		fprintf(stderr, "Memory error\n");
		return FAIL;
	}

	buffer = get_board(dimension);
	if (!buffer) {
		printf("Error loading a board\n");
		free_place(place);
		return FAIL;
	}
	set_place(place, buffer, dimension, 0);

	init_queue(q, cmp_wn, free_wn);
	if (!(*q)) {
		fprintf(stderr, "Failed to initialise queue\n");
		free_place(place);
		return FAIL;
	}

	w_hashset_init(w_hs, delta, diff);
	if (!(*w_hs)) {
		fprintf(stderr, "Failed to initialise w_hashset\n");
		free_place(place);
		free_queue(*q);
		*q = NULL;
		return FAIL;
	}
	if (!insert_node(*q, place)) {
		free_place(place);
		free_queue(*q);
		*q = NULL;
		free_w_hashset(*w_hs);
		*w_hs = NULL;
		return FAIL;
	}

	if (!w_hashset_insert(*w_hs, place)) {
		free_queue(*q);
		*q = NULL;
		free_w_hashset(*w_hs);
		*w_hs = NULL;
		return FAIL;	
	}

	return SUCCESS;
}


w_hashset_ptr read_database(char *fname, int delta, int diff)
{
	FILE *file = NULL;
	w_hashset_ptr w_hs = NULL;
	unsigned long a;
	int b;
	unsigned long *A;
	int *B;


	w_hashset_init(&w_hs, delta, diff);
	file = fopen(fname, "r");

	if (!file) {
		printf("file didn't open?\n");
		w_hs = create_tree(4, DEFAULT_DELTA, DEFAULT_DIFF);
		return w_hs;
	}

	while (2 == fscanf(file, "%lu %d\n", &a, &b)) {
		A = malloc(sizeof(unsigned long));
		B = malloc(sizeof(int));
		*A = a;
		*B = b;
		
		if (ht_insert(w_hs->ht, (void *)A, (void *)B)) {
			fprintf(stderr, "This is weird when inserting into hs\n");
		}
	}

	fclose(file);

	return w_hs;
}

/**
 * The workhorse function that actually explores all the possible 
 * states.
 */
void build_database(queue_t *q, w_hashset_ptr w_hs)
{
	place_t *node = NULL;

#ifdef DEBUG
	int depth = 0;
	printf("Finding Solution:\n");
#endif

	while (get_node_count(q)) {
		node = NULL;
		node = pop_first(q);
		if (!node) {
			fprintf(stderr, 
					"queue empty??? find_puzzle_solution failed to pop a node.\n");
		}

#ifdef DEBUG
		count_all_nodes++;
		if (node->move_count > depth) {
			depth = node->move_count;
			printf("Evaluating %d move cases\n", depth);
		}
#endif

		move_spread_insert(q, w_hs, node, UP);
		move_spread_insert(q, w_hs, node, DOWN);
		free_place(node);	
	}
}

/**
 *  Take a place_t, attempt to move in direction dir, if it works, generate
 *  boards with the space at each of the possible column positions and put 
 *  this result in the queue. 
 */
void move_spread_insert(queue_t *q, w_hashset_ptr w_hs, place_t *node, 
		int dir)
{
	place_t *next = NULL;
	place_t *temp = NULL;
	place_t *side = NULL;
	int i;

	if (dir == node->last_move_inv) {
		return;
	}

	next = new_place();
	if (!next) {
		fprintf(stderr, "Memory error\n");
	}
	copy_place(node, next);
	if (!place_move(next, dir)) {
		free_place(next);
		return;
	}

	if (w_hashset_insert(w_hs, next)) {
		temp = new_place();
		if (!temp) {
			fprintf(stderr, "Memory error\n");
		}
	
		side = new_place();
		if (!side) {
			fprintf(stderr, "Memory error\n");
		}

		copy_place(next, temp);
		for (i = 0; i < temp->w_state.blanc_col; i++) {
			if (!place_move(next, LEFT)) {
				fprintf(stderr, "weird logical error1\n");
			}
			copy_place(next, side);
			insert_node(q, side);
			side = NULL;
			side = new_place();
			if (!side) {
				fprintf(stderr, "memory error while trying moves\n");
			}
		}
		copy_place(temp, next);
		for (i = temp->w_state.blanc_col; i < next->w_state.dimension - 1;
				i++) {
			if (!place_move(next, RIGHT)) {
				fprintf(stderr, "weird logical error2\n");
			}
			copy_place(next, side);
			insert_node(q, side);
			side = NULL;
			side = new_place();
			if (!side) {
				fprintf(stderr, "memory error while trying moves\n");
			}
		}
		copy_place(temp, next);
		insert_node(q, next);
		next = NULL;
	}
	if (next) {
		free_place(next);
	}
	if (temp) {
		free_place(temp);
	}
	if (side) {
		free_place(side);
	}
}

/*################ Queue Functions ######################################*/

/**
 * Compare to queue entries.
 *
 * @param n1 The first of the two entries to be compared.
 * @param n2 The second of the two entries to be compared.
 */
int cmp_wn(void *n1, void *n2)
{
	place_t *a = n1;
	place_t *b = n2;

	return (a->move_count - b->move_count);
}

/** 
 * A function to free an entry in the queue. 
 *
 * @param n The entry to be free'd
 */
void free_wn(void *n)
{
	place_t *place = n;

	free_place(place);
}

/**
 * Print an entry in the queue. 
 *
 * @param n An entry in the queue to be printed. 
 */
void print_wn(void *n) 
{
	print_place((place_t *)n);
}


