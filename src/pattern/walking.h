#ifndef WALKING_H
#define WALKING_H

#include "w_hashset.h"

/*** Function Prototypes *************************************************/

/** 
 * Generate a hashset containing a database of walking distances for
 * given state paterns.  
 *
 * @param N The dimension for which the database must be generated.
 *
 * @return a w_hashset pointer for the set containing the database.
 */
w_hashset_ptr get_walking_database(int N);


/** 
 * Generate a hashset containing a database of walking distances for
 * given state paterns, with given hashtable parameters
 *
 * @param N		The dimension for which the database must be generated.
 * @param delta	The initial delta into the unverlying hashtable.
 * @param diff	The increment by which to increase the hashtable when
 *				resizing.
 *
 * @return a w_hashset pointer for the set containing the database.
 */
w_hashset_ptr get_parameter_walking_database(int N, int delta, int diff);

/** 
 * Generate a hashset containing a database of walking distances for
 * given state paterns, with given hashtable parameters
 *
 * @param N		The dimension for which the database must be generated.
 * @param delta	The initial delta into the unverlying hashtable.
 * @param diff	The increment by which to increase the hashtable when
 *				resizing.
 *
 * @return a w_hashset pointer for the set containing the database.
 */
w_hashset_ptr generate_parameter_walking_database(int N, int delta, int diff);

#endif
