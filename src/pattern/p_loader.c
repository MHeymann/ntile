#include <stdio.h>
#include <stdlib.h>

#include "p_loader.h"

/*** Helper Function Prototypes ******************************************/

char *get_board_fringe(int N);
char *get_board_block(int N);

/*** Functions ***********************************************************/

/**
 * Generate a board that has a dimesion of N.
 * The caller is responsible for freeing the newly allocated goard.
 *
 * @param N The dimension of the new board.
 *
 * @return	A pointer to a char array, where the chars are treated as 
 *			integer numbers.
 */
char *get_board(int N, int type) 
{
	char *board = NULL;
	
	if (type == FRINGE) {
		board = get_board_fringe(N);
	} else if (type == BLOCK) {
		board = get_board_block(N);
	}
	return board;
}

/**
 * Generate a board that has a dimesion of N and only entries at the 
 * fringe.
 * The caller is responsible for freeing the newly allocated goard.
 *
 * @param N The dimension of the new board.
 *
 * @return	A pointer to a char array, where the chars are treated as 
 *			integer numbers.
 */
char *get_board_fringe(int N) 
{
	int i = 0;
	int j = 0;
	char *board = NULL;
	
	if (N < 0) {
		printf("please give a valid argument N to get_board in loader.\n");
		return NULL;
	}

	board = malloc(N * N * sizeof(char));
	if (!board) {
		fprintf(stderr, "Memory error in loader\n");
		return NULL;
	}

	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			if ((i * N + j) < N) {
				board[i * N + j] = (char)(i * N + j + 1);
			} else if (((i * N + j) % N) == 0) {
				board[i * N + j] = (char)(i * N + j + 1);
			} else {
				board[i * N + j] = -1;
			}
		}
	}
	board[N * N - 1] = '\0';

	return board;
}

/**
 * Generate a board that has a dimesion of N and only entries not at the 
 * fringe.
 * The caller is responsible for freeing the newly allocated goard.
 *
 * @param N The dimension of the new board.
 *
 * @return	A pointer to a char array, where the chars are treated as 
 *			integer numbers.
 */
char *get_board_block(int N)
{
	int i = 0;
	int j = 0;
	char *board = NULL;
	
	if (N < 0) {
		printf("please give a valid argument N to get_board in loader.\n");
		return NULL;
	}

	board = malloc(N * N * sizeof(char));
	if (!board) {
		fprintf(stderr, "Memory error in loader\n");
		return NULL;
	}

	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			if ((i * N + j) < N) {
				board[i * N + j] = -1;
			} else if (((i * N + j) % N) == 0) {
				board[i * N + j] = -1;
			} else {
				board[i * N + j] = (char)(i * N + j + 1);
			}
		}
	}
	board[N * N - 1] = '\0';

	return board;
}
/*** Helper Functions ****************************************************/


