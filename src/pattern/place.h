#ifndef GAME_H
#define GAME_H

#include "w_state.h"

/*** Macros **************************************************************/

#define MOVEBUFFER 80

/*** Typedefinitions *****************************************************/

typedef struct place {
	w_state_t w_state;
	int move_count;
	int last_move_inv;
} place_t;

/*** Function Prototypes *************************************************/

/**
 * Allocate memory for a new place.  
 *
 * @return The newly allocated place.
 */
place_t *new_place();

/**
 * Set the board to the place and initiate the fields. 
 *
 * @param board The integers that make up the place, represented as chars.  
 *
 * @param N The dimension of the board. 
 */
void set_place(place_t *place, char *board, int N, int move_count);

/**
 * Make a copy of the provided place.   
 *
 * @param original The place to be copied.
 *
 * @return A pointer to the newly allocated copy of place
 */
void copy_place(place_t *original, place_t *copy);

/**
 * Calculate the lower bound of moves that a given place will be solved in.
 * This includes the moves made up until now. 
 *
 * @param place The place in question.  
 */
int lower_bound(place_t *place);

/** Print a given place.  
 *
 * @param place The place to print. 
 */
void print_place(place_t *place);

/**
 * Make a move on the blanc space in the place.  
 *
 * @param place The place in question.
 *
 * @param dir	The directin to move the blanc space, UP, DOWN, LEFT or 
 *				RIGHT as described int w_state.h
 * 
 * @return SUCCESS (1) if the move was carried out successfully
 * and FAIL (0) if not.  
 */
int place_move(place_t *place, int dir);

/**
 * Free a place and its underlying datastructures.  
 *
 * @param place The place to be freed.  
 */
void free_place(place_t *place);

#endif
