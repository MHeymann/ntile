#include <stdio.h>
#include <stdlib.h>

#include "w_hashset.h"
#include "loader.h"

#define TRUE  1
#define FALSE 0

/*** Helper Function Prototypes ******************************************/

int assert_alloc(void *data, char *name); 

/*** Main Routine ********************************************************/

int main(int argc, char *argv[])
{
	char *buffer = NULL;
	int N = 0;
	place_t *place = NULL;
	place_t *cp_place = NULL;
	w_hashset_ptr w_hs = NULL;
	int ret_status = 0;

	if (argc != 2) {
		printf("Usage: %s <number>\n", argv[0]);
		printf("Where <number> is the dimension of the board\n");
		return 1;
	}

	N = atoi(argv[1]);
	buffer = get_board(N);
	if (!assert_alloc(buffer, "buffer")) {
		ret_status = 2;
		goto CLEANUP;
	}
	
	place = new_place();
	if (!assert_alloc(place, "place")) {
		ret_status = 3;
		goto CLEANUP;
	}
	set_place(place, buffer, N, 0);	
	buffer = NULL;


	w_hashset_init(&w_hs, 3, 2);
	if (!w_hs) {
		printf("w_hashset_init() failed");
		free(buffer);
		free_place(place);
		ret_status = 4;
		goto CLEANUP;
	}

	ret_status = w_hashset_insert(w_hs, place);
	if (!ret_status) {
		printf("Failed to insert initial place\n");
	}
	cp_place =  new_place();
		copy_place(place, cp_place); 

	if (!w_hashset_insert(w_hs, cp_place)) {
		printf("Good! duplicate caught.\n");
	} else {
		printf("error, inserted the same place twice\n");
		print_w_hashset(w_hs);
	}

	place_move(cp_place, UP);
	place_move(cp_place, LEFT);

	if (w_hashset_insert(w_hs, cp_place)) {
		printf("Inserted\n");
	} else {
		printf("error, failed to insert\n");
		print_w_hashset(w_hs);
	}	

	print_w_hashset(w_hs);

	ret_status = 0;
CLEANUP:
	if (place) {
		free_place(place);
	}
	if (cp_place) {
		free_place(cp_place);
	}
	if (buffer) {
		free(buffer);
	}
	if (w_hs) {
		free_w_hashset(w_hs);
	}

	return ret_status;
}

/*** Helper Functions ****************************************************/

int assert_alloc(void *data, char *name)
{
	if (!data) {
		printf("failed to initialize %s\n", name);
		return FALSE;
	} else {
		return TRUE;
	}
}
