#include <stdio.h>
#include <stdlib.h>

#include "loader.h"
#include "place.h"
#include "../queue.h"

/*** Helper Function Prototypes ******************************************/

void test_all(place_t *place, int dir);
int cmp_g(void *g1, void *g2);
void free_g(void *n);
void print_g(void *n);
void do_some_moves_and_insert(queue_t *q, place_t *n, int no_of_moves, 
		int *directions);

/*** Main Routine ********************************************************/

int main(int argc, char *argv[])
{
	int N = 0;
	int i;
	int count;
	char *buffer = NULL;
	place_t *place1 = NULL;
	place_t *place2 = NULL;
	place_t *place3 = NULL;
	queue_t *q = NULL;
	int directions1[1] = {DOWN};
	int directions2[1] = {LEFT};
	int directions3[5] = {LEFT, LEFT, LEFT, LEFT, LEFT};
	int directions4[8] = {UP, UP, UP, UP, UP, LEFT, DOWN, RIGHT};
	int directions5[1] = {RIGHT};
	int directions6[1] = {UP};

	if (argc != 2) {
		printf("usage: %s <dimension>\n", argv[0]);
		exit(1);
	}

	N = atoi(argv[1]);
	buffer = get_board(N);

	place1 = new_place();
	set_place(place1, buffer, N, 0);
	place2 = new_place();
	copy_place(place1, place2);

	init_queue(&q, cmp_g, free_g);

	/* Try popping from a queue with nothing in it.  */
	if ((place3 = pop_first(q))) {
		printf("This is weird! %p\n", (void *)place3);
	} else {
		printf("Nice!  doesn't break upon poping from empty queue\n");
	}

	/* 
	 * Lets enter some stuff into the queue. 
	 * Entering 8 stuff into the queue.  Change the macro 
	 * for the initial size of the queue to 5 to make sure the 
	 * resizing works when the queue gets full. 
	 */
	printf("About to insert first entry\n");
	if (insert_node(q, place1) == FAIL) {
		printf("Failed to insert\n");
		free(place1);
	}
	printf("Just inserted the following place:\n");
	print_place(place1);
	printf("\n");
	place1 = new_place();
	copy_place(place2, place1);

	do_some_moves_and_insert(q, place1, 1, directions1);

	place1 = new_place();
	copy_place(place2, place1);
	do_some_moves_and_insert(q, place1, 1, directions2);
	
	place1 = new_place();
	copy_place(place2, place1);
	do_some_moves_and_insert(q, place1, 5, directions3);

	place1 = new_place();
	copy_place(place2, place1);
	do_some_moves_and_insert(q, place1, 8, directions4);

	place1 = new_place();
	copy_place(place2, place1);
	do_some_moves_and_insert(q, place1, 1, directions2);

	place1 = new_place();
	copy_place(place2, place1);
	do_some_moves_and_insert(q, place1, 1, directions5);
	
	place1 = new_place();
	copy_place(place2, place1);
	do_some_moves_and_insert(q, place1, 1, directions6);

	/* 
	 * show the original to ensure that the original doesn't 
	 * accidentally get tampered with.  
	 */
	printf("The copy of the original:\n");
	print_place(place2);
	printf("\n");

	/* Keep place1 as a copy */
	place1 = new_place();
	copy_place(place2, place1);

	/* free place2 so can be popped into */
	free_place(place2);
	count = get_node_count(q);
	print_queue(q, print_g);
	/* test whether emptying the queue works */
	empty_queue(q);

	/* 
	 * now that it is empty, check that popping from the empty queue
	 * doesn't cause problems. 
	 */
	for (i = 0; i < count + 1; i++) {
		printf("%d of %d) popping and printing:\n", i + 1, count + 1);
		if ((place2 = pop_first(q))) {
			print_place(place2);
			printf("\n");
			free_place(place2);
		} else {
			printf("seems empty\n\n");
		}
	}

	/* Check whether one can still add stuff to the emptied queue */
	place2 = new_place();
	copy_place(place1, place2);
	if (insert_node(q, place1) == FAIL) {
		printf("Could not insert into the queue!\n");
		free_place(place1);
	}

	place1 = new_place();
	copy_place(place2, place1);
	do_some_moves_and_insert(q, place1, 8, directions4);

	place1 = new_place();
	copy_place(place2, place1);
	do_some_moves_and_insert(q, place1, 1, directions2);
	
	place1 = new_place();
	copy_place(place2, place1);
	do_some_moves_and_insert(q, place1, 5, directions3);

	place1 = new_place();
	copy_place(place2, place1);
	do_some_moves_and_insert(q, place1, 1, directions6);

	/* print this queue now */
	print_queue(q, print_g);

	/* Free data structures */
	free_place(place2);
	free_queue(q);

	/* no segfaults or early exits */
	printf("exiting normally\n");
	return 0;
}

/*** Helper Functions ****************************************************/

void test_all(place_t *place, int dir)
{
	printf("Moving the blanc space ");
	switch (dir) {
		case LEFT:
			printf("left\n");
			break;
		case RIGHT:
			printf("right\n");
			break;
		case UP:
			printf("up\n");
			break;
		case DOWN:
			printf("down\n");
			break;
		default:
			fprintf(stderr, 
					"You called test_all with an invalid direction parameter\n");
	}
	if (!place_move(place, dir)) {
		printf("Caught a none move\n");
	}
	/*
	print_place(place);
	*/
	printf("\n");
}

void do_some_moves_and_insert(queue_t *q, place_t *n, int no_of_moves, 
		int *directions)
{
	int i; 

	for (i = 0; i < no_of_moves; i++) {
		test_all(n, directions[i]);
	}
	if (insert_node(q, n) == FAIL) {
		printf("Failed to insert node!\n");
		free_place(n);
		n = NULL;
	} else {
		print_place(n);
		printf("\n");
	}
}

int cmp_g(void *g1, void *g2)
{
	place_t *a = g1;
	place_t *b = g2;

	/*TODO: this is crucial */
	return (a->move_count - b->move_count);
}


void free_g(void *n)
{
	place_t *place = n;

	free_place(place);
}


void print_g(void *n) 
{
	print_place((place_t *)n);
}
