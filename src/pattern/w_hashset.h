/*
 * This lookuptable is specifically for getting a databasis for use in the
 * walking distance heuristic with the aim
 * of solving an n-puzzle.
 */
#ifndef w_LOOKUPTABLE_H
#define w_LOOKUPTABLE_H

#include "place.h"
#include "../hashtable.h"

#define TRUE	1
#define SUCCESS	1
#define FALSE	0
#define FAIL	0

typedef struct w_hashset *w_hashset_ptr;


/*** Lookuptable struct Description **************************************/

typedef struct w_hashset {
	hashtable_p ht;
} w_hashset_t;

/*** Function Prototypes *************************************************/

/**
 * Allocate the datastructures of a w_hashset and initialise the parameters.
 *
 * @param[in][out] ht		a pointer to a pointer of the newly created 
 *							w_hashset.
 * @param[in]	init_delta	The initial delta into the hashtable's delta
 *							table.
 * @param[in]	delta_diff	The increment by which to increase the index
 *							into the delta table when resizing the
 *							hashtable.
 */
void w_hashset_init(w_hashset_ptr *w_hs, int init_delta, int delta_diff);

/**
 * Insert a place instance into the w_hashset.
 *
 * @param[in] w_hs	A pointer to the w_hashset into which to insert.
 * @param[in] place A pointer to the place instance to insert.
 *
 * @return SUCCESS(1) if the place was inserted successfully and
 * FAIL(0) if not.
 */
int w_hashset_insert(w_hashset_ptr w_hs, place_t *place);

/**
 * look up an id, and if found, put its number of moves into value.
 *
 * @param[in] lu The table to search in.
 * @param[in] id The id at a given place.
 *
 * @param[out] value An address of an int where the walking distance of 
 * stored place id will be put
 *
 * @return TRUE (1) if found and FALSE (0) if not.
 */
int w_hashset_lookup(w_hashset_ptr w_hs, unsigned long id, int *value);

/**
 *	Print the underlying hashtable of the w_hashset. 
 *
 *	@param[in] w_hs A pointer to the w_hashset to print.  
 */
void print_w_hashset(w_hashset_ptr w_hs);

/**
 *	Print the entries in the w_hashset in no specific order.
 *
 *	@param[in] w_hs A pointer to the w_hashset to print.  
 */
void print_w_hashset_entries(w_hashset_ptr w_hs);

/**
 * Free the values in the w_hashset and the w_hashset itself. 
 *
 * @param[in] w_hs The w_hashset to be free'd.  
 */
void free_w_hashset(w_hashset_ptr w_hs);

#endif
