#ifndef W_STATE_H
#define W_STATE_H

#define DOWN	0
#define LEFT	1
#define UP		2
#define RIGHT	3

/*** Macros **************************************************************/

#define SUCCESS 1
#define FAIL	0
#define TRUE	1
#define FALSE	0

/*** Type Definitions ****************************************************/

typedef struct w_state {
	char *board;
	int dimension;
	int blanc_row;
	int blanc_col;
} w_state_t;

/*** Function Prototypes *************************************************/

/**
 * allocate a new w_state_t structure.
 *
 * @return the newly allocated structure.  
 */
w_state_t *new_w_state();

/**
 * Set a given board of integers to the game w_state, and fill in 
 * the various fields of game w_state as needed.  
 *
 * @param w_state	The w_state to be set to. 
 * @param board		The integers in the given game w_state.  
 *
 * @param N The dimension of the board, ie the number of integers
 * in a row or a column.  
 */
void w_set_board(w_state_t *w_state, char *board, int N);

/** 
 * copy from one w_state to another.
 *
 * @param w_state		The w_state to be copied
 * @param w_state_copy	The w_state to be copied to.  
 */
void copy_w_state(w_state_t *w_state, w_state_t *copy);

/**
 * Free the datastructures in as w_state and the w_state itself.  
 *
 * @param w_state The w_state to be freed.  
 */
void free_w_state(w_state_t *w_state);

/**
 * Move the blanc space in a given direction.  
 *
 * @param w_state	The w_state which must be altered.  
 * @param direction The direction in which the blanc space must move. 
 *					UP, DOWN, LEFT or RIGHT as described in w_state.h
 *
 * @return SUCCESS (1) if the move is possible and FAIL (0) if not. 
 */
int w_make_move(w_state_t *w_state, int direction);

/**
 * Calculate the id number for a given w_state.
 * creates an id number for this w_state that can be used in a hash set or
 * similar.  
 * 
 * @param w_state The w_state in question.  
 *
 * @return The id value. 
 */
unsigned long w_get_id(w_state_t *w_state);

/**
 * print the w_state and all its accompanying values.  
 *
 * @param w_state The w_state in question.
 */
void print_w_state(w_state_t *w_state);


#endif
