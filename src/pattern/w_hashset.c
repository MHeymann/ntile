#include <stdio.h>
#include <stdlib.h>

#include "../hashtable.h"
#include "w_hashset.h"
#include "w_state.h"

/*** Helper Function Prototypes ******************************************/

unsigned long hash_place(void *key, unsigned int size);
int cmp_place(void *a, void *b);
void w_val2str(void *key, void *val, char *buffer);

/*** Functions ***********************************************************/

/**
 * Allocate the datastructures of a w_hashset and initialise the parameters.
 *
 * @param[in][out] w_ht a pointer to a pointer of the newly created w_hashset.
 * @param[in]	init_delta	The initial delta into the hashtable's delta
 *							table.
 * @param[in]	delta_diff	The increment by which to increase the index
 *							into the delta table when resizing the
 *							hashtable.
 */
void w_hashset_init(w_hashset_ptr *w_hs, int init_delta, int delta_diff)
{
	hashtable_p ht = NULL;
	w_hashset_ptr hset = NULL;

	hset = malloc(sizeof(w_hashset_t));
	if (!hset) {
		fprintf(stderr, "Error allocating memory for lookuptable.\n");
		*w_hs = NULL;
		return;
	}

	ht = ht_init(0.75, init_delta, delta_diff, hash_place, cmp_place);
	if (!ht) {
		fprintf(stderr, "Error initializing hashtable.\n");
		free(hset);
		*w_hs = NULL;
		return;
	}

	hset->ht = ht;

	*w_hs = hset;
}

/**
 * Insert a place instance into the w_hashset.
 *
 * @param[in] w_hs A pointer to the w_hashset into which to insert.
 *
 * @param[in] place A pointer to the place instance to insert.
 *
 * @return SUCCESS(1) if the place was inserted successfully and
 * FAIL(0) if not.
 */
int w_hashset_insert(w_hashset_ptr w_hs, place_t *place)
{
	int insert_status;
	unsigned long *id = NULL;
	int *moves = NULL;

	id = malloc(sizeof(unsigned long));
	if (!id) {
		fprintf(stderr, "Memory error 1 in w_hashset_insert\n");
		return FAIL;
	}

	moves = malloc(sizeof(int));
	if (!moves) {
		fprintf(stderr, "Memory error 2 in w_hashset_insert\n");
		free(id);
		return FAIL;
	}

	*id = w_get_id((w_state_t *)place);
	*moves = place->move_count;

	insert_status = ht_insert(w_hs->ht, (void *)id, (void *)moves);
	if (insert_status) {
		free(moves);
		free(id);
		switch (insert_status) {
			case 1:
				fprintf(stderr, 
						"Memory error when inserting into hashtable\n");
				break;
			case KEY_PRESENT_IN_TABLE:
				/*just means it is present.*/
#ifdef DEBUGHS
				fprintf(stdout, "already in queue\n");
#endif
				break;
			default:
				fprintf(stderr, 
						"This is weird in w_hashset_insert table's switch w_statement: %d\n", 
						insert_status);
				break;
		}
		return FAIL;
	} else {
		return SUCCESS;
	}
}

/**
 * look up an id, and if found, put its number of moves into value.
 *
 * @param[in] lu The table to search in.
 * @param[in] id The id at a given place.
 *
 * @param[out] value An address of an int where the walking distance of 
 * stored place id will be put
 *
 * @return TRUE (1) if found and FALSE (0) if not.
 */
int w_hashset_lookup(w_hashset_ptr w_hs, unsigned long id, int *value)
{
	int lookup_result = -1;
	int *get_val = NULL;

	lookup_result = ht_lookup(w_hs->ht, (void *)&id, (void **)&get_val);

	if (lookup_result) {
		*value = *get_val;
		return TRUE;
	} else {
		*value = -1;
		return FALSE;
	}
}



/**
 *	Print the underlying hashtable of the w_hashset. 
 *
 *	@param[in] w_hs A pointer to the w_hashset to print.  
 */
void print_w_hashset(w_hashset_ptr w_hs)
{
	print_ht(w_hs->ht, w_val2str);	
}

/**
 *	Print the entries in the w_hashset in no specific order.
 *
 *	@param[in] w_hs A pointer to the w_hashset to print.  
 */
void print_w_hashset_entries(w_hashset_ptr w_hs)
{
	print_ht_entries(w_hs->ht, w_val2str);	
}

/**
 * Free the values in the w_hashset and the w_hashset itself. 
 *
 * @param[in] w_hs The w_hashset to be free'd.  
 */
void free_w_hashset(w_hashset_ptr w_hs)
{
	ht_free(w_hs->ht, free, free);
	w_hs->ht = NULL;
	free(w_hs);
}


/*** Helper Functions ****************************************************/

/**
 * Get a string representation of a key value pair, to be put in buffer.
 *
 * @param[in] key The unique key of the pair, used to hash with.   
 *
 * @param[in] val The value of the pair.
 *
 * @param[out] buffer The char pointer where the string representation 
 * of the pair will be put.  
 */
void w_val2str(void *key, void *val, char *buffer)
{
	unsigned long *k = (unsigned long *)key;
	int *v = (int *) val;
	sprintf(buffer, "%lu %d", *k, *v);	
}

/**
 * A hash function to be used when placing places in the hashtable.  
 *
 * @param[in] key The key to be used to hash with.  
 *
 * @param[in] size The size of the table into which to put the 
 * key value pair.  
 *
 * @return The hash value as an unsigned long. 
 */
unsigned long hash_place(void *key, unsigned int size)
{
    unsigned long *id = (unsigned long *)key;
	unsigned long hash = *id % size;

	return hash;
}

/**
 * A comparison function used to compare two places when trying 
 * to put a value into the hashtable. 
 *  @param[in] a A place pointer
 *
 *  @param[in] b A place pointer
 *
 *  @return An integer value, 0 if the two places are in an identical
 *  w_state, less than 0 if a comes before b and more than 0 otherwise. 
 */
int cmp_place(void *a, void *b)
{
	unsigned long *A = (unsigned long *)a;
	unsigned long *B = (unsigned long *)b;

	if (*A > *B) {
		return 1;
	} else if (*A < *B) {
		return -1;
	} else {
		return 0;
	}
}

