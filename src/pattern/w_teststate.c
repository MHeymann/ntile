#include <stdio.h>
#include <stdlib.h>

#include "loader.h"
#include "w_state.h"

void test_all(w_state_t *w_state, int dir);

int main(int argc, char *argv[])
{
	int N = 0;
	char *buffer = NULL;
	w_state_t *w_state;
	w_state_t *copy;

	if (argc != 2) {
		printf("usage: %s <dimension>\n", argv[0]);
		exit(1);
	}

	N = atoi(argv[1]);
	buffer = get_board(N);
	if (!buffer) {
		printf("Failed to load board\n");
		exit(2);
	}

	w_state = new_w_state();
	copy = new_w_state();
	w_set_board(w_state, buffer, N);
	copy_w_state(w_state, copy);
	
	print_w_state(w_state);
	test_all(w_state, LEFT);
	test_all(w_state, UP);
	test_all(w_state, RIGHT);
	test_all(w_state, DOWN);

	printf("The copy of the original:\n");
	print_w_state(copy);

	free_w_state(w_state);
	free_w_state(copy);

	return 0;
}

void test_all(w_state_t *w_state, int dir) 
{
	printf("Moving the blanc space ");
	switch (dir) {
		case LEFT:
			printf("left\n");
			break;
		case RIGHT:
			printf("right\n");
			break;
		case UP:
			printf("up\n");
			break;
		case DOWN:
			printf("down\n");
			break;
		default:
			fprintf(stderr, "You called test_all with an invalid direction parameter\n");
	}
	w_make_move(w_state, dir);
	print_w_state(w_state);
	printf("\n");
}


