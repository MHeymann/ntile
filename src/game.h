#ifndef GAME_H
#define GAME_H

#include "state.h"

/*** Macro Definitions ***************************************************/

#define MOVEBUFFER 80

/*** Struct Definition ***************************************************/

typedef struct game {
	state_t state;
	unsigned char move_count;
	unsigned char lowerb;
	char *moves;
	unsigned char current_buffer;
	char last_move_inv;
	struct game *parent;
} game_t;

/*** Function Prototypes *************************************************/

/**
 * Allocate memory for a new game.  
 *
 * @return The newly allocated game.
 */
game_t *new_game();

/**
 * Set the board to the game and initiate the fields. 
 *
 * @param board		 The integers that make up the game.  
 *
 * @param N			 The dimension of the board. 
 *
 * @param move_count The integer number of moves made up until this point.
 *
 * @param moves		 Moves made up to this point, given if now parent is
 *					 known or available.
 * @param heuristic	 An integer that indicates the heuristic that will 
 *					 be used on this game.
 */
void set_game(game_t *game, char *board, char N, int move_count, 
		char *moves, int heuristic);

/**
 * Copy from one game to another.  
 *
 * @param original	The game to be copied.
 *
 * @param copy		The game to which to copy.  
 */
void copy_game(game_t *original, game_t *copy);

/**
 * Calculate the lower bound of moves that a given game will be solved in.
 * This includes the moves made up until now. 
 *
 * @param game The game in question.  
 */
int lower_bound(game_t *game, char heuristic);

/** Print a given game.  
 *
 * @param game The game to print. 
 */
void print_game(game_t *game);

/**
 * Make a move on the blanc space in the game.  
 *
 * @param game	The game in question.
 *
 * @param dir	The directin to move the blanc space, UP, DOWN, LEFT or 
 *				RIGHT as described int state.h
 * 
 * @return		SUCCESS (1) if the move was carried out successfully
 *				and FAIL (0) if not.  
 */
int game_move(game_t *game, char dir, char heuristic);

/** 
 * Function to make a string of the moves followed to a given point
 * recursively called on each of a nodes parent.  
 * The string is stored in the string called buffer.  The caller
 * is responsible to ensure that buffer has enough space.  
 *
 * @param[in] game		The current game position for which all prior
 *						moves must be obtained.  
 *
 * @param[out] buffer	The string in which the result must be stored.  
 */
void get_solution_string_recursively(game_t *game, char *buffer);

/**
 * Free a game and its underlying datastructures.  
 *
 * @param game The game to be freed.  
 */
void free_game(game_t *game);

#endif
