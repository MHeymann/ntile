# README #

### What is this repository for? ###

This is the semester project for RW314 at Stellenbosch University in 2016.
It solves a given n-puzzle, provided that it is solvable and that the system
has the memory capacity required. 
It contains both a serial and a parallel implementation.

### How do I get set up? ###

The source files are all found in the src directory tree.  The walking distance
heuristic is implemented in the directory walking, found in src. 
Make sure the directories obj and obj/walking exist.

To make both programs, run the following:

	$ chmod 700 make.sh
	$ ./make.sh

To compile the programs such that debugging information gets printed when
running the programs, run the following:

	$ chmod 700 dbg_make.sh
	$ ./dbg_make.sh

To make the testprograms, run the following:

	$ make tests

### How to run ###

The programs can be run with the run.sh script.  Initially, make sure
run.sh is executable.

	$ chmod 700 run.sh

Then to run the programs:

	$ ./run <program> <options>

One of two programs must be specified - either "serialsolver" or "parallelsolver"
If serialsolver is specified, it must be immediately followed by the path to an
inputfile as an argument.  Further options available for serial solver are as
follows: 
	
	-delta\_init=<n>	This indicates the index that determines the initial size 
						of the hashtable to be used to avoid duplicates. <n> must 
						be an integer number between 0 and 32.  The hashtable size 
						will then be roughly 2^n entries. (default value is 17)

	-delta\_diff=<n>	This indicates by which value the delta index must be 
						incremented when resizing the hashtable. (default value 
						is 2)

	-heur=<h>			This indicates the heuristic to be used in the priority
						queue, where <h> is either "man" for manhattan or "walk"
						for walking distance. (the default is walking distance)

If parallelsolver was specified, then it must be followed immediately by an
integer number, the number of threads to run.  This is then followed by a path
to an input file, with the following further options that may follow:

	-delta\_init=<n>	This indicates the index that determines the initial size 
						of the hashtable to be used to avoid duplicates. <n> must 
						be an integer number between 0 and 32.  The hashtable size 
						will then be roughly 2^n entries. (default value is 17)

	-delta\_diff=<n>	This indicates by which value the delta index must be 
						incremented when resizing the hashtable. (default value 
						is 2)

	-heur=<h>			This indicates the heuristic to be used in the priority
						queue, where <h> is either "man" for manhattan or "walk"
						for walking distance. (the default is walking distance)

	-c\_interval=<n>	<n> indicates the number of seconds between
						communication between threads.

	-t_interval=<n>		<n> is the number of seconds between checking for the
						termination signal.

	--neighbour			Specifies that the inial problem must be expanded to
						have at least as many children as there are threads,
						and if it is not solved yet, these children are to be
						divided amongst the threads, which will then each 
						solve its subproblem independently. (This is the 
						default approach)

	--passer			Specifies that subproblems must be passed around
						during every communitation period.  

	If the last two options are both specified, the last one will be acted upon.

In all cases, inputfile must consist of N lines, each with N integers, seperated
by spaces

### General Info ###

This program should solve a sliding tile puzzle.  The parser detects
automatically what the dimension of the puzzle is, assuming the input is a
square of numbers between 1 and N^2 - 1, if N is the dimension of one row.  
0 represents a gap where no tile is present. More information can be found
on this wikipedia page:

	https://en.wikipedia.org/wiki/15_puzzle

The longest possible 3 puzzle solution is 31 single tile moves and the 
longest possible 4 puzzle has a shortest solution of 81 moves.

To test if a given puzzle is solvable, the following website provides the
background to a working algorithm:

	https://www.cs.bham.ac.uk/~mdr/teaching/modules04/java2/TilesSolvability.html

### Some Specifics ###

The programs are an implementation of the a\* algorithm:  A breadth first tree
search algorithm that uses a priority queue ordered by an estimated lower bound
function.  
This function uses some heuristic to estimate the numver of moves
left for a given state, and adds to that the number of moves already made.  Only
heuristic function that can be proven to estimate moves less than or equal to
the actual solution may be used.  Such a heuristic is called 'admissible'.  
If a heuristic is not admissable, the algorithm is not guaranteed to give the
shortest solution. Both the manhattan distance and the walking distance of a
given state is admissable, with walking distance always being greater than or 
equal to manhattan distance.  This makes walkding distance a better heuristic. 

### Acknowledgements ###

I used Willem Bester's hashtable, as used in last semesters project, as 
a basis for my hashset.  I did recreate the hashtable myself, but it
very closesly resembles his and I cannot and do not claim it to be my 
own work.  
Furthermore, the priority queue used is based on the Java implementation
given in Sedgewick and Wayne's Algorithms book.  I translated it to C
and adapted some of it to the needs of this project. 

### Who do I talk to? ###

Email:
	
	heymann.murray@gmail.com 
	15988694@sun.ac.za

Cellphone:
	
	yeah, right.
