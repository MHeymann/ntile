#! /bin/bash

if [ "$1" = "serialsolver" ]
then
	shift
	echo "time ./serialsolver $*"
	time ./serialsolver $*
elif [ "$1" = "parallelsolver" ]
then
	shift
	N=$1
	shift
	echo "time mpiexec -n $N ./parallelsolver $*"
	time mpiexec -n $N ./parallelsolver $*
else
	echo "Usage: $0 <progname> <args>"
	echo "Where <progname> is either \"serialsolver\" or \"parallelsolver <n>\"."
	echo "If parallelsolver \"parallelsolver <n>\" is chosen, then <n> denotes"
	echo "the number of threads."
	echo "<args> are the appropriate arguments for the chosen program"
fi
