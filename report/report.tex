
\title{N-tile puzzle solver}
\author{
        Murray Heymann \\
		15988694 \\
                Department of Mathematical Sciences\\
                Division Computer Science\\
        Stellenbosch University\\
        Victoria Street, Stellenbosch 7600, \underline{South Africa}
}
\date{\today}

\documentclass[12pt]{article}

\usepackage{listings}
\usepackage{epigraph}

\begin{document}
\maketitle

\epigraph{White shores, and beyond, a far green country under a swift
sunrise.}{J.R.R. Tolkien}

\begin{abstract}
This is the semester project for RW314 at Stellenbosch University in 2016. The
module focus is on concurrency and the end goal was to create a program that
can solve a given n-puzzle with multiple threads using MPI.  
\end{abstract}

\section{Introduction}
The use of concurrency is becoming increasingly common and a good understanding of its
principles is vital to any modern programmer.  This project is an attempt to 
parallelize the problem of finding the solution to a given n-puzzle that requires
smallest number of single tile moves with MPI.  The resultant program will then be
analyzed for scalability and its suitability for parallelization with MPI will
be considered.

\section{Code Overview}
First a serial version was written and optimized, and this was used to build a
parallel version. The serial version is an implementation of the well known a*
algorithm with the following pseudocode, using either Manhattan distance or a 
heuristic known as Walking distance added to the number of moves already
performed on a given state to order the priority queue with:

\begin{lstlisting}[caption=Serial\ Solution, label=amb]
q       : A priority queue.
hs      : A hash set to keep game states that have been visited. 
start, n: Game states.

/* The main routine */
main(args)
begin
    start<-loadproblem()
    solution<-find_solution(start)
    print_solution(solution)
end

/* The work horse function to find a solution */
find_solution(start)
begin
    initiate(q)	
    initiate(hs)
    insert(q, start)

    while (TRUE) 
    begin
        n<-pop_first(q)
        if (is_solved(n))
            return to_string(n)
        else
            try_insert(hs, q, children(n))
        fi
    end
end

/* 
 * A function that checks the hash set before 
 * inserting into the queue
 */
try_insert(hs, q, n)
begin
    if (not contains(hs, n))
        insert(q, n)
    fi
end

\end{lstlisting}

The heuristic used to order the priority queue must be guaranteed to always give 
an estimate of the number of moves left that is less than or equal to the actual number 
of moves in the solution.  Such a heuristic is called "admissible".

The children function expands a puzzle state by performing all possible moves except
for the inverse of the move that was performed on the given state's parent to 
produce that state previously. In short, back moves are avoided.  This prunes the 
search tree, saves memory and make the number of entries in the priority queue
fewer. To keep track of the nodes that have already been visited, 
all nodes are placed in a hash set.  This also allows for easy once-off freeing at 
the end. Every node gets a pointer to its parent and stores the inverse of the 
move made on the parent to produce itself.  This setup also allows a solution string 
to be generated recursively. 

For the parallel solution, each thread would explore a subproblem.  If more than 
one problem is present in a priority queue, one gets sent to another thread at 
a set interval. 

A token-based control ring approach to termination was followed.  A token gets
passed around from one thread to the next in order of thread rank, with the
highest ranking thread sending it back to thread 0.  The token also carries a string
with the moves of the best solution found so far and a count of messages sent.
If the token is received and carries a solution with fewer moves than the 
lower bound of the first item in that thread's priority queue, the thread's queue empties. This 
ensures once the best solution was found, other threads don't keep searching if
all solutions found further on are guaranteed by the heuristic function to be longer 
than the solution
found.  The token also relays information about whether any threads are still
busy. Every time a node in the priority queue gets explored by a thread, a 
flag is set.  If this flag is set when the token comes around, the token gets 
marked "black" and sent along.  Thread 0 clears the token every round to white.
At every node, it adds a count of the number of messages it sent to other
threads to the token's count and subtract the number it received. Termination 
occurs if the token reaches thread 0 still marked as white, thread 0's flag is not
set as busy and the sum of messages sent and received is 0.  Thread 0 then
sends the termination signal to all other threads.

The following Pseudocode was my first implementation, which I called a "Passer
Approach," a reference to the passing of subproblems between different threads.

\begin{lstlisting}[caption=First\ Parallel\ Solution, label=amb]

main(args)
begin
    if (my_rank == 0)
        start<-loadproblem()
    else
        start<-NULL
    fi
    solution<-find_solution(start)
    print_solution
end

find_solution(start)
begin
    initiate(q)	
    initiate(hs)
    insert(q, start)
    my_colour<-WHITE

    while (TRUE) 
    begin
        if (is_empty(q)) 
            order = communicate()
            if (order == STOP)
                return solution
            fi
        fi
        if (now() - last_comm_time < interval)
            order = communicate() 
            if (order == STOP)
                return solution
            fi
        fi
        if (is_empty(q))
            continue
        fi
        n = pop_first(q)
        if (is_solved(n))
            if (length(n) < best_length)
                solution<-to_string(n)
                best_length<-length(n)
            fi
        else
            my_colour<-BLACK
            try_insert(hs, q, children(n))
        fi
    end
end

communicate()
begin
    if (message tagged TERMINATE)
        return STOP
    fi
    if (message tagged TOKEN)
        receive_token()
        if (token.slength < best_length)
            best_length<-token.slength
            solution<-token.solution
        fi
        if (token.slength > best_length)
            token.slength<-best_length
            token.solution<-solution
        fi
        if (not is_empty(q) AND best_length < first(q).estimate)
            empty(q)
        fi
        if (my_rank == 0)
            if (token.colour == WHITE AND my_colour == WHITE 
                AND msg_count == 0)
                send_terminate_message(all)
                return STOP
            fi
            token.colour<-WHITE
            my_colour<-WHITE       
        elif (my_colour == BLACK)
            token.colour<-BLACK
            my_colour<-WHITE
        send_token(successor(my_rank))
    fi
    if (message tagged problem)
        receive problem
        insert(q, problem)
    fi
    
    if (get_count(q) > 1) 
        n = pop_first(q)
        send_problem(n, successor(my_rank))
    fi
    return GO_ON
end

try_insert(hs, q, n)
begin
    if (not contains(hs, n))
        insert(q, n)
    fi
end
\end{lstlisting}

This approach was slow and initially tended to grind to a crawl. 
Therefore, another approach was tried, where
the original problem was distributed to every thread.  Each thread
expanded the node into a game tree until nodes in the queue numbered at
least as many as there are threads running.  If no solution was found
during this expansion, each thread took the node at the same index position
in the priority queue as its own rank and used that node to find a serial
solution.  Once a thread finds a solution, this is handed to the others 
via the token that gets passed in a circle. Termination happened with the same
token based approach as before. 

\begin{lstlisting}[caption=Second\ Parallel\ Solution, label=amb]
main(args)
begin
    if (my_rank == 0)
        start<-loadproblem()
        Broadcast(start, 0)
    else
        Broadcast(start, 0)
    fi
    solution<-find_solution(start)
    print_solution(solution)
end

find_solution(start)
begin
    initiate(q)	
    initiate(hs)
    x<-get_child_n(start, my_rank)
    insert(q, x)
    my_colour<-WHITE
    while (TRUE) 
    begin
        if (is_empty(q)) 
            order = communicate()
            if (order == STOP)
                return solution
            fi
        fi
        if (now() - last_comm_time < interval)
            order = communicate() 
            if (order == STOP)
                return solution
            fi
        fi
        if (is_empty(q))
            continue
        fi
        n = pop_first(q)
        if (is_solved(n))
            if (length(n) < best_length)
                solution<-to_string(n)
                best_length<-length(n)
            fi
        else
            my_colour<-BLACK
            try_insert(hs, q, children(n))
        fi
    end
end

communicate()
begin
    if (message tagged TERMINATE)
        return STOP
    fi
    if (message tagged TOKEN)
        receive_token()
        if (token.slength < best_length)
            best_length<-token.slength
            solution<-token.solution
        fi
        if (token.slength > best_length)
            token.slength<-best_length
            token.solution<-solution
        fi
        if (not is_empty(q) AND best_length < first(q).estimate)
            empty(q)
        fi
        if (my_rank == 0)
            if (token.colour == WHITE AND my_colour == WHITE 
                AND msg_count == 0)
                send_terminate_message(all)
                return STOP
            fi
            token.colour<-WHITE
            my_colour<-WHITE       
        elif (my_colour == BLACK)
            token.colour<-BLACK
            my_colour<-WHITE
        send_token(successor(my_rank))
    fi
    return GO_ON
end

try_insert(hs, q, n)
begin
    if (not contains(hs, n))
        insert(q, n)
    fi
end
\end{lstlisting}

This approach, which I called the "neighbouring approach," proved to work 
somewhat better and more reliably.  Program execution is deterministic with 
this approach, and communication kept at a minimum. However, one still has 
the problem that the different threads cannot see the hash set contents of the 
other threads, so duplication between the different threads are possible. This 
means the memory footprint is much larger than a serial solution's.

\section{Results}\label{results}
For the trivial problem, both approaches proved much faster 
with a single thread than running with multiple.  This is due to the overhead
associated with creating multiple threads and setting up the MPI 
infrastructure in the background. When running with one thread, only 0.08
seconds was required, compared to just over one second for any number of
threads greater than 1. From this can be deduced that about 1 second is lost 
to setting up threads and creating background data structures in MPI.  
One or two move problems had the same pattern, as did any problem of a 3 by 3
board.  As even the longest 3 by 3 problem takes only a few milliseconds with a
single thread, all further testing was done with 4 by 4 problems.  

The passer approach solved a given 46 move problem (random15\_1.txt) in 0.45
seconds with a single thread.  With multiple threads, accounting for overhead 
lets one expect computation time of about 1.4 seconds, which is indeed the 
time 2 threads take. 3 threads took 2 seconds and 4 threads 2.4 seconds. This 
increase in computation time can be attributed to the lack of a shared 
hash table.  This means that different threads might each reach the same state, 
and both explore the resultant children of that state, adding at least one 
unnecessary branch that expands exponentially.  Furthermore the frequency of 
communication increases with increased threads, which further slows down the
system. Finally, with the  circular token-termination, more nodes means it 
takes longer for the token to get the best solution to all threads, and it then 
takes longer for thread 0 to send the termination signal. On a 58 move 
(random15\_4.txt) problem, it took a single thread 18 seconds to solve, and 17
for two threads. Three threads took 20 seconds and four threads 28 seconds. 

It is clear from these results that the passer approach doesn't scale well. 

The neighbour approach was tried with the 46 move (random15\_1.txt) puzzle.
A single thread solved this in 0.58 seconds. Two threads took 1.367 seconds,
somewhat better than one thread when considering that about a second was added 
for overhead. Three threads took 1.5 seconds and four took 1.6 seconds. The
longer 58 move (random15\_4.txt) puzzle took 21.5 seconds to solve with a
single tread. Two threads took 16.4 seconds and three threads took 22.2 seconds.  
Four threads took 25 seconds. The improvement in performance between one and two
threads is noteworthy, but it is also noted that performance degraded severely 
after that. This is likely due to the increased termination cost with 
increasing numbers of threads.

The scalability of the program could not be accurately determined, as the 
problem size is difficult to determine. Time and memory requirements of a 
serial program tends to grow exponentially as the number of steps increase,
however these also related to the value heuristic function for each state
along the optimal solution path. So it can happen that a 50 move 
(random15\_3.txt) solution can take much quicker (0.1 seconds) than for 
instance a 46 move (random15\_1.txt) puzzle (0.59 seconds), with a close 
relation between the time taken and the memory requirement.

However, since time taken tends to increase with more threads, suffice it 
to say that this program is very weakly scalable.  The nature of this problem,
with its exponentially growing game tree and with MPI making different
threads's memory banks (with each thread's hash set) virtually unreachable to 
other threads all make me conclude that this problem is a bad candidate for 
parallelization using my approaches.  A different implementation with a shared 
memory system, a single hash set and one priority queue, might be a better 
approach, since the single hash set would eliminate duplication and simplify 
termination detection.  

\section{Conclusions}\label{conclusions}

With any problem that needs to be parallelized, it is worth asking whether 
the problem is worth parallelizing.  And if so, what method of parallelization
might work best.  Implementing a parallel n-puzzle solver with MPI scales very
badly and might be better approached using pthreads or openmp.

\end{document}
This is never printed
  
